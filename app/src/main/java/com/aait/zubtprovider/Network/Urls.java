package com.aait.zubtprovider.Network;

public class Urls {
    public final static String baseUrl = "https://zubt.aait-sa.com/api/";

    public final static String Terms = "about-and-terms";
    public final static String ContactInfo = "contact-info";
    public final static String COntactUs = "contact-us";
    public final static String Register = "register";
    public final static String Activate = "active-user";
    public final static String Login = "sign-in";
    public final static String FOrgotPassword = "forget-password";
    public final static String ResetPassword = "reset-password";
    public final static String Profile = "profile";
    public final static String LogOut = "logout";
    public final static String ChangePassword = "change-password";
    public final static String Repeated = "questions";
    public final static String Cities = "get-cities";
    public final static String Category = "categories";
    public final static String ProviderInfo = "provider-info";
    public final static String EditProfile = "edit-profile";
    public final static String Home = "provider/home";
    public final static String OrderInfo = "order-info";
    public final static String AcceptOrder = "provider/accept-order";
    public final static String RefuseOrder = "provider/refuse-order";
    public final static String Notification = "notifications";
    public final static String Finish = "provider/finish-order";
    public final static String Orders = "provider/orders";
    public final static String removeNotification = "remove-notification";
    public final static String Balance = "provider/balance";
    public final static String BankAccounts = "bank-accounts";
    public final static String Pay = "provider/pay";



}
