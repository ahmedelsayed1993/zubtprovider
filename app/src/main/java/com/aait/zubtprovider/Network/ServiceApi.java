package com.aait.zubtprovider.Network;



import com.aait.zubtprovider.Models.BankResponse;
import com.aait.zubtprovider.Models.BaseResponse;
import com.aait.zubtprovider.Models.CityResponse;
import com.aait.zubtprovider.Models.ContactInfoResponse;
import com.aait.zubtprovider.Models.FinancialResponse;
import com.aait.zubtprovider.Models.LoginResponse;
import com.aait.zubtprovider.Models.NotificationResponse;
import com.aait.zubtprovider.Models.OrderDetailsResponse;
import com.aait.zubtprovider.Models.OrdersResponse;
import com.aait.zubtprovider.Models.ProviderDetialsResponse;
import com.aait.zubtprovider.Models.QuestionResponse;
import com.aait.zubtprovider.Models.RegisterResponse;
import com.aait.zubtprovider.Models.TermsResponse;

import java.util.List;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface ServiceApi {

    @GET(Urls.Terms)
    Call<TermsResponse> getTerms(@Header("lang") String lang);

    @POST(Urls.ContactInfo)
    Call<ContactInfoResponse> getContacts();

    @POST(Urls.COntactUs)
    Call<BaseResponse> Contact(@Header("lang") String lang,
                               @Query("name") String name,
                               @Query("email") String email,
                               @Query("message") String message);
    @Multipart
    @POST(Urls.Register)
    Call<RegisterResponse> register(@Header("lang") String lang,
                                    @Query("name") String name,
                                    @Query("phone") String phone,
                                    @Query("email") String email,
                                    @Query("password") String password,
                                    @Query("user_type") String user_type,
                                    @Query("lat") String lat,
                                    @Query("lng") String lng,
                                    @Query("city_id") int city_id,
                                    @Query("category_id") int category_id,
                                    @Query("location_ar") String location_ar,
                                    @Query("location_en") String location_en,
                                    @Part MultipartBody.Part national_id);

    @POST(Urls.Activate)
    Call<BaseResponse> Activate(@Header("lang") String lang,
                                @Header("token") String token,
                                @Query("code") String code);

    @POST(Urls.Login)
    Call<LoginResponse> Login(@Header("lang") String lang,
                              @Query("phone") String phone,
                              @Query("password") String password,
                              @Query("device_id") String device_id,
                              @Query("device_type") String device_type,
                              @Query("user_type") String user_type);

    @POST(Urls.FOrgotPassword)
    Call<LoginResponse> forgotPass(@Header("lang") String lang,
                                   @Query("phone") String phone,
                                   @Query("user_type") String user_type);
    @POST(Urls.ResetPassword)
    Call<BaseResponse> resetPassword(@Header("lang") String lang,
                                     @Header("token") String token,
                                     @Query("password") String password,
                                     @Query("code") String code);

    @POST(Urls.Profile)
    Call<LoginResponse> getProfile(@Header("lang") String lang,
                                   @Header("token") String token);
    @POST(Urls.LogOut)
    Call<BaseResponse> logout(@Header("token") String token,
                              @Query("device_id") String device_id);
    @POST(Urls.ChangePassword)
    Call<BaseResponse> changePass(@Header("lang") String lang,
                                  @Header("token") String token,
                                  @Query("old_password") String old_password,
                                  @Query("password") String password);
    @GET(Urls.Repeated)
    Call<QuestionResponse> getQuestions(@Header("lang") String lang,
                                        @Query("page") int page);

    @GET(Urls.Cities)
    Call<CityResponse> getCity(@Header("lang") String lang);

    @GET(Urls.Category)
    Call<CityResponse> getCategory(@Header("lang") String lang);

    @POST(Urls.ProviderInfo)
    Call<ProviderDetialsResponse> getProvider(@Header("lang") String lang,
                                              @Header("token") String token,
                                              @Query("provider_id") String provider_id);
    @Multipart
    @POST(Urls.EditProfile)
    Call<LoginResponse> editWithAvatar(@Header("token") String token,
                                       @Query("lang") String lang,
                                       @Query("name") String name,
                                       @Query("phone") String phone,
                                       @Query("email") String email,
                                       @Query("category_id") int category_id,
                                       @Query("city_id") int city_id,
                                       @Query("work_from") String work_from,
                                       @Query("work_to") String work_to,
                                       @Query("details") String details,
                                       @Part MultipartBody.Part avatar,
                                       @Query("location_ar") String location_ar,
                                       @Query("location_en") String location_en,
                                       @Query("lat") String lat,
                                       @Query("lng") String lng);
    @POST(Urls.EditProfile)
    Call<LoginResponse> edit(@Header("token") String token,
                             @Query("lang") String lang,
                             @Query("name") String name,
                             @Query("phone") String phone,
                             @Query("email") String email,
                             @Query("category_id") int category_id,
                             @Query("city_id") int city_id,
                             @Query("work_from") String work_from,
                             @Query("work_to") String work_to,
                             @Query("details") String details,
                             @Query("location_ar") String location_ar,
                             @Query("location_en") String location_en,
                             @Query("lat") String lat,
                             @Query("lng") String lng);
    @POST(Urls.EditProfile)
    Call<LoginResponse> lang(@Header("token") String token,
                             @Query("lang") String lang);
    @POST(Urls.Home)
    Call<OrdersResponse> getHome(@Header("lang") String lang,
                                 @Header("token") String token,
                                 @Query("page") int page);
    @POST(Urls.OrderInfo)
    Call<OrderDetailsResponse> getOrder(@Header("lang") String lang,
                                        @Header("token") String token,
                                        @Query("order_id") String order_id);

    @POST(Urls.AcceptOrder)
    Call<BaseResponse> accept(@Header("lang") String lang,
                              @Header("token") String token,
                              @Query("order_id") int order_id,
                              @Query("cost") String cost,
                              @Query("include_material") int include_material,
                              @Query("include_tax") int include_tax,
                              @Query("provider_notes") String provider_notes);
    @POST(Urls.RefuseOrder)
    Call<BaseResponse> Refuse(@Header("lang") String lang,
                              @Header("token") String token,
                              @Query("order_id") String order_id);

    @POST(Urls.Finish)
    Call<BaseResponse> finish(@Header("lang") String lang,
                              @Header("token") String token,
                              @Query("order_id") String order_id);

    @POST(Urls.Notification)
    Call<NotificationResponse> getNotification(@Header("lang") String lang,
                                               @Header("token") String token,
                                               @Query("page") int page);

    @POST(Urls.Orders)
    Call<OrdersResponse> getOrders(@Header("lang") String lang,
                                   @Header("token") String token,
                                   @Query("type") String type,
                                   @Query("page") int page);

    @POST(Urls.removeNotification)
    Call<BaseResponse> remove(@Header("token") String token,
                              @Query("notification_id") int notification_id);
    @POST(Urls.Balance)
    Call<FinancialResponse> getBalance(@Header("lang") String lang,
                                       @Header("token") String token);
    @POST(Urls.BankAccounts)
    Call<BankResponse> getBanks(@Header("lang") String lang,
                                @Header("token") String token);

    @Multipart
    @POST(Urls.Pay)
    Call<BaseResponse> pay(@Header("lang") String lang,
                           @Header("token") String token,
                           @Query("bank_name") String bank_name,
                           @Query("owner_name") String owner_name,
                           @Query("account_id") String account_id,
                           @Query("amount") String amount,
                           @Part MultipartBody.Part invoice);



}
