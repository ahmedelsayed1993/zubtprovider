package com.aait.zubtprovider.Listeners;

/**
 * Created by Ahmed El_sayed on 16/1/2019.
 */

public interface SnackBarClickListener {
    void onSnackBarActionClickListener();
}
