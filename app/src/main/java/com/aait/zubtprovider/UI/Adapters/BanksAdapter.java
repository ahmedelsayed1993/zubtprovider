package com.aait.zubtprovider.UI.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.aait.zubtprovider.Base.ParentRecyclerAdapter;
import com.aait.zubtprovider.Base.ParentRecyclerViewHolder;
import com.aait.zubtprovider.Models.Bank_model;
import com.aait.zubtprovider.Models.OrderModel;
import com.aait.zubtprovider.R;
import com.bumptech.glide.Glide;

import java.util.List;

import butterknife.BindView;

public class BanksAdapter extends ParentRecyclerAdapter<Bank_model> {
    public BanksAdapter(Context context, List<Bank_model> data, int layoutId) {
        super(context, data, layoutId);
    }

    @NonNull
    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(mcontext).inflate(layoutId, viewGroup, false);
        BanksAdapter.ViewHolder holder = new BanksAdapter.ViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ParentRecyclerViewHolder parentRecyclerViewHolder, int i) {
        final BanksAdapter.ViewHolder viewHolder = (BanksAdapter.ViewHolder) parentRecyclerViewHolder;
        final Bank_model categoryModel = data.get(i);
        viewHolder.bank_name.setText(": "+categoryModel.getBank_name());
        viewHolder.account_number.setText(": "+categoryModel.getAccount_id());
        viewHolder.account_name.setText(": "+categoryModel.getAccount_name());
        viewHolder.iban_name.setText(": "+categoryModel.getIban_id());
    }
    public class ViewHolder extends ParentRecyclerViewHolder {

        @BindView(R.id.bank_name)
        TextView bank_name;

        @BindView(R.id.account_name)
        TextView account_name;
        @BindView(R.id.account_number)
        TextView account_number;
        @BindView(R.id.iban_name)
        TextView iban_name;






        ViewHolder(View itemView) {
            super(itemView);
        }

    }
}
