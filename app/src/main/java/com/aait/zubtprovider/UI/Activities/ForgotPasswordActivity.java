package com.aait.zubtprovider.UI.Activities;

/**
 * created by ahmed el_sayed  7/10/2019
 */

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;

import com.aait.zubtprovider.Base.ParentActivity;
import com.aait.zubtprovider.Models.LoginResponse;
import com.aait.zubtprovider.Network.RetroWeb;
import com.aait.zubtprovider.Network.ServiceApi;
import com.aait.zubtprovider.R;
import com.aait.zubtprovider.Uitls.CommonUtil;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPasswordActivity extends ParentActivity {
    @BindView(R.id.phone)
    EditText phone;
    @OnClick(R.id.confirm)
    void onConfirmClick(){
        if (CommonUtil.checkTextError((AppCompatActivity)mContext,phone,getString(R.string.enter_phone))){
            return;
        }else {
            ForGotPass();
        }
    }
    @Override
    protected void initializeComponents() {

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_forgot_password;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }

    private void ForGotPass(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).forgotPass(mLanguagePrefManager.getAppLanguage(),phone.getText().toString(),"client").enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getValue().equals("1")){
                        Intent intent = new Intent(mContext,RecoverPasswordActivity.class);
                        intent.putExtra("token",response.body().getData().getApi_token());
                        intent.putExtra("code",response.body().getData().getCode());
                        startActivity(intent);
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
}
