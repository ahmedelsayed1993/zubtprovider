package com.aait.zubtprovider.UI.Activities;

import android.content.Intent;
import android.widget.TextView;

import com.aait.zubtprovider.Base.ParentActivity;
import com.aait.zubtprovider.Models.FinancialResponse;
import com.aait.zubtprovider.Network.RetroWeb;
import com.aait.zubtprovider.Network.ServiceApi;
import com.aait.zubtprovider.R;
import com.aait.zubtprovider.Uitls.CommonUtil;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FinancialAccountsActivity extends ParentActivity {
    @OnClick(R.id.act_back)
    void onBack(){
        onBackPressed();
    }
    @OnClick(R.id.notification)
    void onNotification(){
        startActivity(new Intent(mContext,NotificationActivity.class));
    }
    @BindView(R.id.act_title)
    TextView act_title;
    @BindView(R.id.balance)
    TextView balance;
    @Override
    protected void initializeComponents() {
        act_title.setText(getString(R.string.financial_accounts));
        getBalance();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_finicial_accounts;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }
    private void getBalance(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getBalance(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getApi_token()).enqueue(
                new Callback<FinancialResponse>() {
                    @Override
                    public void onResponse(Call<FinancialResponse> call, Response<FinancialResponse> response) {
                        hideProgressDialog();
                        if (response.isSuccessful()){
                            if (response.body().getValue().equals("1")){
                                balance.setText(response.body().getData().getBalance()+"");
                            }else {
                                CommonUtil.makeToast(mContext,response.body().getMsg());
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<FinancialResponse> call, Throwable t) {
                        CommonUtil.handleException(mContext,t);
                        t.printStackTrace();
                        hideProgressDialog();

                    }
                }
        );
    }
    @OnClick(R.id.pay)
    void onPay(){
        startActivity(new Intent(mContext,BankAccountsActivity.class));
    }
}
