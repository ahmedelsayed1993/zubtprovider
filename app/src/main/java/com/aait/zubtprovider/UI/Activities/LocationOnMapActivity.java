package com.aait.zubtprovider.UI.Activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.aait.zubtprovider.App.Constant;
import com.aait.zubtprovider.Base.ParentActivity;
import com.aait.zubtprovider.Gps.GPSTracker;
import com.aait.zubtprovider.Gps.GpsTrakerListener;
import com.aait.zubtprovider.R;
import com.aait.zubtprovider.Uitls.CommonUtil;
import com.aait.zubtprovider.Uitls.DialogUtil;
import com.aait.zubtprovider.Uitls.PermissionUtils;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;

public class LocationOnMapActivity extends ParentActivity implements OnMapReadyCallback, GoogleMap.OnMapClickListener, GpsTrakerListener {
    @BindView(R.id.map)
    MapView mapView;



    GoogleMap googleMap;

    Marker myMarker;

    GPSTracker gps;

    public String mLang, mLat;
    String id;

    private AlertDialog mAlertDialog;

    boolean startTracker = false;
    @OnClick(R.id.act_back)
    void onBack(){
        onBackPressed();
    }
    @BindView(R.id.act_title)
    TextView act_title;
    @OnClick(R.id.notification)
    void onNotification(){
        startActivity(new Intent(mContext,NotificationActivity.class));
    }

    @Override
    protected void initializeComponents() {

        mLat = getIntent().getStringExtra("lat");
        mLang = getIntent().getStringExtra("lng");
        id = getIntent().getStringExtra("id");
        act_title.setText(getString(R.string.order_details));
        mapView.onCreate(mSavedInstanceState);
        mapView.onResume();
        mapView.getMapAsync(this);

        try {
            MapsInitializer.initialize(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_location_on_map;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }

    @Override
    public void onTrackerSuccess(Double lat, Double log) {
        Log.e("Direction", "Direction Success");
        // dismiss traker dialog
        if (startTracker) {
            if (lat != 0.0 && log != 0.0) {
                hideProgressDialog();
                Log.e("LATLNG", "Lat:" + mLat + "  Lng:" + Double.toString(log));
                putMapMarker(lat, log);
            }
        }
    }

    @Override
    public void onStartTracker() {
        startTracker = true;

    }

    @Override
    public void onMapClick(LatLng latLng) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        googleMap.setOnMapClickListener(this);
        putMapMarker(Double.parseDouble(mLat),Double.parseDouble(mLang));
    }
    public void getLocationWithPermission() {
        gps = new GPSTracker(this, this);
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!PermissionUtils.hasPermissions(LocationOnMapActivity.this, PermissionUtils.GPS_PERMISSION)) {
                CommonUtil.PrintLogE("Permission not granted");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(PermissionUtils.GPS_PERMISSION,
                            Constant.RequestPermission.REQUEST_GPS_LOCATION);
                    Log.e("GPS", "1");
                }
            } else {
                getCurrentLocation();
                Log.e("GPS", "2");
            }
        } else {
            Log.e("GPS", "3");
            getCurrentLocation();
        }

    }

    void getCurrentLocation() {
        gps.getLocation();
        if (!gps.canGetLocation()) {
            mAlertDialog = DialogUtil.showAlertDialog(LocationOnMapActivity.this,
                    getString(R.string.gps_detecting), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(final DialogInterface dialogInterface, final int i) {
                            mAlertDialog.dismiss();
                            Intent intent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            startActivityForResult(intent, Constant.RequestCode.GPS_ENABLING);
                        }
                    });
        } else {
            if (gps.getLatitude() != 0.0 && gps.getLongitude() != 0.0) {
                putMapMarker(gps.getLatitude(), gps.getLongitude());

                googleMap.clear();
                putMapMarker(Double.parseDouble(mLat), Double.parseDouble(mLang));
            }
        }
    }
    public void putMapMarker(Double lat, Double log) {
        Log.e("LatLng:", "Lat: " + lat + " Lng: " + log);
        // getLocationInfo("" + lat, "" + log, "ar");
        LatLng latLng = new LatLng(lat, log);
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
        MarkerOptions marker = new MarkerOptions().position(
                new LatLng(lat, log));
        marker.icon(BitmapDescriptorFactory
                .fromResource(R.mipmap.maps_and_flags));

        myMarker = googleMap.addMarker(marker);
    }

}
