package com.aait.zubtprovider.UI.Activities;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;

import com.aait.zubtprovider.App.Constant;
import com.aait.zubtprovider.Base.ParentActivity;
import com.aait.zubtprovider.Listeners.OnItemClickListener;
import com.aait.zubtprovider.Models.CityModel;
import com.aait.zubtprovider.Models.CityResponse;
import com.aait.zubtprovider.Models.LoginResponse;
import com.aait.zubtprovider.Models.UserModel;
import com.aait.zubtprovider.Network.RetroWeb;
import com.aait.zubtprovider.Network.ServiceApi;
import com.aait.zubtprovider.R;
import com.aait.zubtprovider.UI.Views.ChangePasswordDiolg;
import com.aait.zubtprovider.UI.Views.ListDialog;
import com.aait.zubtprovider.Uitls.CommonUtil;
import com.aait.zubtprovider.Uitls.PermissionUtils;
import com.aait.zubtprovider.Uitls.ProgressRequestBody;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.fxn.pix.Pix;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import gun0912.tedbottompicker.TedBottomPicker;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.aait.zubtprovider.App.Constant.RequestPermission.REQUEST_IMAGES;

public class ProfileActivity extends ParentActivity implements ProgressRequestBody.UploadCallbacks, OnItemClickListener {
    @OnClick(R.id.act_back)
    void onBack(){
        startActivity(new Intent(mContext,ProviderDetailsActivity.class));
        ProfileActivity.this.finish();
    }
    @BindView(R.id.act_title)
    TextView act_title;
    ChangePasswordDiolg changePasswordDiolg;
    @OnClick(R.id.change_pass)
    void onChangePass(){
        changePasswordDiolg = new ChangePasswordDiolg(mContext);
        changePasswordDiolg.show();
    }
    @BindView(R.id.request_location)
    TextView request_location;
    String mAdresse="", mLang=null, mLat = null, mAddress="",address = "";
    @OnClick(R.id.notification)
    void onNotification(){
        startActivity(new Intent(mContext,NotificationActivity.class));
    }
    @BindView(R.id.user_name)
    EditText user_name;
    @BindView(R.id.phone)
    EditText phone;
    @BindView(R.id.email)
    EditText email;
    @BindView(R.id.civ_user_image)
    CircleImageView civ_user_image;
    ArrayList<Uri> ImageList = new ArrayList<>();

    String ImageBasePath = null;
    @BindView(R.id.date)
    TextView date;
    @BindView(R.id.time)
    TextView time;
    @BindView(R.id.category)
    TextView category;
    @BindView(R.id.city)
    TextView city1;
    @BindView(R.id.description)
    EditText description;
    ArrayList<CityModel> cityModels = new ArrayList<>();
    ListDialog listDialog;
    CityModel city;
    ArrayList<CityModel> categoryModels = new ArrayList<>();
    CityModel category1;
    int selected = 0;
    @BindView(R.id.name)
    TextView name;

    @Override
    protected void initializeComponents() {
        act_title.setText(getString(R.string.profile));
        getProfile();

    }
    @OnClick(R.id.request_location)
    void onLocation(){
        MapDetectLocationActivity.startActivityForResult((AppCompatActivity)mContext);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_profile;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }
    void SetData(UserModel userModel){
        Glide.with(mContext).load(userModel.getAvatar()).apply(new RequestOptions().placeholder(R.mipmap.untitled_6).fitCenter()).into(civ_user_image);
        user_name.setText(userModel.getName());
        name.setText(userModel.getName());
        phone.setText(userModel.getPhone());
        email.setText(userModel.getEmail());
        category.setText(userModel.getCategory_name());
        city1.setText(userModel.getCity_name());
        time.setText(userModel.getWork_to());
        date.setText(userModel.getWork_from());
        description.setText(userModel.getDetails());
        request_location.setText(userModel.getLocation());
        city = new CityModel(userModel.getCity_id(),userModel.getCity_name());
        category1 = new CityModel(userModel.getCategory_id(),userModel.getCategory_name());

    }
    @OnClick(R.id.civ_user_image)
    void onImage(){
        getPickImageWithPermission();
    }
    public void getPickImageWithPermission() {
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext,PermissionUtils.IMAGE_PERMISSIONS))) {
                CommonUtil.PrintLogE("Permission not granted");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(PermissionUtils.IMAGE_PERMISSIONS,
                            REQUEST_IMAGES);
                }
            } else {
                Pix.start(this, REQUEST_IMAGES, 1);
                CommonUtil.PrintLogE("Permission is granted before");
            }
        } else {
            CommonUtil.PrintLogE("SDK minimum than 23");
            Pix.start(this, REQUEST_IMAGES, 1);
        }
    }
    @OnClick(R.id.date)
    void onDate(){
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                date.setText( selectedHour + ":" + selectedMinute);
            }
        }, hour, minute, false);
        //Yes 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();
    }
    @OnClick(R.id.time)
    void onTime(){
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                time.setText( selectedHour + ":" + selectedMinute);
            }
        }, hour, minute, false);
        //Yes 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();

    }
    void pickMultiImages() {
        TedBottomPicker bottomSheetDialogFragment = new TedBottomPicker.Builder(mContext)
                .setOnMultiImageSelectedListener(new TedBottomPicker.OnMultiImageSelectedListener() {
                    @Override
                    public void onImagesSelected(ArrayList<Uri> uriList) {
                        ImageList = uriList;
                        ImageBasePath = ImageList.get(0).getPath();
                        civ_user_image.setImageURI(Uri.parse(ImageBasePath));

                    }
                })
                .setTitle("avatar")
                .setSelectMaxCount(1)
                .setSelectMinCount(1)
                .setPeekHeight(2600)
                .showTitle(false)
                .setCompleteButtonText(R.string.choose)
                .setEmptySelectionText(R.string.no_image)
                .create();
        bottomSheetDialogFragment.show(getSupportFragmentManager());
    }

    private void getProfile(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getProfile(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getApi_token()).enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getValue().equals("1")){
                        SetData(response.body().getData());

                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }

    private void editWithAvatar(String path){
        showProgressDialog(getResources().getString(R.string.please_wait));
        MultipartBody.Part filePart = null;
        File ImageFile = new File(path);
        ProgressRequestBody fileBody = new ProgressRequestBody(ImageFile, ProfileActivity.this);
        filePart = MultipartBody.Part.createFormData("avatar", ImageFile.getName(), fileBody);
        RetroWeb.getClient().create(ServiceApi.class).editWithAvatar(mSharedPrefManager.getUserData().getApi_token(),mLanguagePrefManager
        .getAppLanguage(),user_name.getText().toString(),phone.getText().toString(),email.getText().toString(),category1.getId(),city.getId(),date.getText().toString(),time.getText().toString(),description.getText().toString(),filePart,mAddress,mAdresse,mLat,mLang).enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getValue().equals("1")){
                        SetData(response.body().getData());
                        mSharedPrefManager.setUserData(response.body().getData());
                        CommonUtil.makeToast(mContext,getString(R.string.your_data_changed));
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    private void edit(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).edit(mSharedPrefManager.getUserData().getApi_token(),mLanguagePrefManager.getAppLanguage(),user_name.getText().toString(),phone.getText().toString(),email.getText().toString(),category1.getId(),city.getId(),date.getText().toString(),time.getText().toString(),description.getText().toString(),mAddress,mAdresse,mLat,mLang).enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getValue().equals("1")){
                        SetData(response.body().getData());
                        mSharedPrefManager.setUserData(response.body().getData());
                        CommonUtil.makeToast(mContext,getString(R.string.your_data_changed));
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    @OnClick(R.id.save)
    void onSave(){
        if (ImageBasePath != null){
            if(CommonUtil.checkTextError((AppCompatActivity)mContext,user_name,getString(R.string.enter_user_name))||
            CommonUtil.checkTextError((AppCompatActivity)mContext,phone,getString(R.string.enter_phone))||
                    CommonUtil.checkLength(phone,getString(R.string.phone_length),9)||
            CommonUtil.checkTextError((AppCompatActivity)mContext,email,getString(R.string.enter_email))||
                    !CommonUtil.isEmailValid(email,getString(R.string.correct_email))||
            CommonUtil.checkTextError(city1,getString(R.string.choose_city))||
            CommonUtil.checkTextError(category,getString(R.string.choose_category))||
                    CommonUtil.checkTextError(request_location,getString(R.string.enter_location))||
            CommonUtil.checkTextError(date,getString(R.string.work_from))||
            CommonUtil.checkTextError(time,getString(R.string.work_to))||
            CommonUtil.checkTextError((AppCompatActivity)mContext,description,getString(R.string.description))){
                return;
            }else {
                editWithAvatar(ImageBasePath);
            }
        }else {
            if(CommonUtil.checkTextError((AppCompatActivity)mContext,user_name,getString(R.string.enter_user_name))||
                    CommonUtil.checkTextError((AppCompatActivity)mContext,phone,getString(R.string.enter_phone))||
                    CommonUtil.checkLength(phone,getString(R.string.phone_length),9)||
                    CommonUtil.checkTextError((AppCompatActivity)mContext,email,getString(R.string.enter_email))||
                    !CommonUtil.isEmailValid(email,getString(R.string.correct_email))||
                    CommonUtil.checkTextError(city1,getString(R.string.choose_city))||
                    CommonUtil.checkTextError(category,getString(R.string.choose_category))||
                    CommonUtil.checkTextError(request_location,getString(R.string.enter_location))||
                    CommonUtil.checkTextError(date,getString(R.string.work_from))||
                    CommonUtil.checkTextError(time,getString(R.string.work_to))||
                    CommonUtil.checkTextError((AppCompatActivity)mContext,description,getString(R.string.description))){
                return;
            }else {
                edit();
            }
        }
    }
    @Override
    public void onProgressUpdate(int percentage) {

    }

    @Override
    public void onError() {

    }

    @Override
    public void onFinish() {

    }
    @OnClick(R.id.city)
    void onCity(){
        selected = 0;
        getCities();
    }
    @OnClick(R.id.category)
    void onCategory(){
        selected = 1;
        getCategory();
    }
    private void getCities(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getCity(mLanguagePrefManager.getAppLanguage()).enqueue(new Callback<CityResponse>() {
            @Override
            public void onResponse(Call<CityResponse> call, Response<CityResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getValue().equals("1")){
                        cityModels = response.body().getData();
                        listDialog = new ListDialog(mContext,ProfileActivity.this,cityModels,getString(R.string.choose_city));
                        listDialog.show();
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<CityResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }

    @Override
    public void onItemClick(View view, int position) {
        listDialog.dismiss();
        if (view.getId() == R.id.tv_row_title){
            if (selected==0) {
                city = cityModels.get(position);
                city1.setText(city.getValue());
            }else {
                category1 = categoryModels.get(position);
                category.setText(category1.getValue());
            }
        }

    }
    private void getCategory(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getCategory(mLanguagePrefManager.getAppLanguage()).enqueue(new Callback<CityResponse>() {
            @Override
            public void onResponse(Call<CityResponse> call, Response<CityResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getValue().equals("1")){
                        categoryModels = response.body().getData();
                        listDialog = new ListDialog(mContext,ProfileActivity.this,categoryModels,getString(R.string.choose_category));
                        listDialog.show();
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<CityResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (data != null) {
            if (requestCode == Constant.RequestPermission.REQUEST_IMAGES){
                ArrayList<String> returnValue = data.getStringArrayListExtra(Pix.IMAGE_RESULTS);

                ImageBasePath =returnValue.get(0);
                Glide.with(mContext).load(ImageBasePath).apply(new RequestOptions().placeholder(R.mipmap.untitled_6).fitCenter().sizeMultiplier(100)).into(civ_user_image);
//                civ_user_image.setImageURI(Uri.parse(ImageBasePath));
            }else if (requestCode == Constant.RequestCode.GET_LOCATION) {
                if (resultCode == RESULT_OK) {
                    mAdresse = data.getStringExtra(Constant.LocationConstant.LOCATION);
                    mAddress = data.getStringExtra("LOCATION");
                    address = data.getStringExtra("LOC");
                    mLang = data.getStringExtra(Constant.LocationConstant.LNG);
                    mLat = data.getStringExtra(Constant.LocationConstant.LAT);
                    CommonUtil.PrintLogE("Lat : " + mLat + " Lng : " + mLang + " Address : " + mAdresse+"  "+mAddress+"  "+address);
                    if (mLanguagePrefManager.getAppLanguage().equals("ar")) {
                        request_location.setText(mAddress);
                    }else if (mLanguagePrefManager.getAppLanguage().equals("en")){
                        request_location.setText(mAdresse);
                    }

                }
            }
        }
    }
}
