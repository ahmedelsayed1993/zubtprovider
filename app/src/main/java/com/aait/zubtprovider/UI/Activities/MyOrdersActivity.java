package com.aait.zubtprovider.UI.Activities;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.widget.TextView;

import com.aait.zubtprovider.Base.ParentActivity;
import com.aait.zubtprovider.R;
import com.aait.zubtprovider.UI.Adapters.SubscribeTapAdapter;

import butterknife.BindView;
import butterknife.OnClick;

public class MyOrdersActivity extends ParentActivity {
    @BindView(R.id.myOrders)
    TabLayout myOrdersTab;

    @BindView(R.id.myOrdersViewPager)
    ViewPager myOrdersViewPager;
    @OnClick(R.id.notification)
    void onNotification(){
        startActivity(new Intent(mContext,NotificationActivity.class));
    }
    @OnClick(R.id.act_back)
    void onBack(){
        onBackPressed();
    }
    @BindView(R.id.act_title)
    TextView act_title;
    private SubscribeTapAdapter mAdapter;
    @Override
    protected void initializeComponents() {
        act_title.setText(getString(R.string.my_orders));
        mAdapter = new SubscribeTapAdapter(mContext,getSupportFragmentManager());
        myOrdersViewPager.setAdapter(mAdapter);
        myOrdersTab.setupWithViewPager(myOrdersViewPager);


    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_orders;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }
}
