package com.aait.zubtprovider.UI.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.aait.zubtprovider.Base.ParentRecyclerAdapter;
import com.aait.zubtprovider.Base.ParentRecyclerViewHolder;
import com.aait.zubtprovider.Models.ReviewsModels;
import com.aait.zubtprovider.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

public class ReviewsAdapter extends ParentRecyclerAdapter<ReviewsModels> {
    public ReviewsAdapter(Context context, List<ReviewsModels> data, int layoutId) {
        super(context, data, layoutId);
    }

    @NonNull
    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(mcontext).inflate(layoutId, viewGroup, false);
        ReviewsAdapter.ViewHolder holder = new ReviewsAdapter.ViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ParentRecyclerViewHolder parentRecyclerViewHolder, int i) {
        final ReviewsAdapter.ViewHolder viewHolder = (ReviewsAdapter.ViewHolder) parentRecyclerViewHolder;
        final ReviewsModels categoryModel = data.get(i);
        viewHolder.name.setText(categoryModel.getName());
        Glide.with(mcontext).load(categoryModel.getAvatar()).apply(new RequestOptions().placeholder(R.mipmap.untitled_6).fitCenter()).into(viewHolder.image);
        viewHolder.time.setText(categoryModel.getCreated_at());
        viewHolder.comment.setText(categoryModel.getComment());
        if (categoryModel.getIs_reported()==0){
            viewHolder.block.setImageDrawable(mcontext.getResources().getDrawable(R.mipmap.flag));
        }else {
            viewHolder.block.setImageDrawable(mcontext.getResources().getDrawable(R.mipmap.flagg));
        }
    }
    public class ViewHolder extends ParentRecyclerViewHolder {

        @BindView(R.id.image)
        CircleImageView image;

        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.time)
        TextView time;
        @BindView(R.id.comment)
        TextView comment;
        @BindView(R.id.block)
        ImageView block;




        ViewHolder(View itemView) {
            super(itemView);
        }

    }
}
