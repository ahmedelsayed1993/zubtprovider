package com.aait.zubtprovider.UI.Activities;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.aait.zubtprovider.App.Constant;
import com.aait.zubtprovider.Base.ParentActivity;
import com.aait.zubtprovider.Models.BaseResponse;
import com.aait.zubtprovider.Models.OrderDetailsModel;
import com.aait.zubtprovider.Models.OrderDetailsResponse;
import com.aait.zubtprovider.Network.RetroWeb;
import com.aait.zubtprovider.Network.ServiceApi;
import com.aait.zubtprovider.R;
import com.aait.zubtprovider.UI.Adapters.ImagesAdapter;
import com.aait.zubtprovider.Uitls.CommonUtil;
import com.aait.zubtprovider.Uitls.PermissionUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderDetailsActivity extends ParentActivity {
    @OnClick(R.id.act_back)
    void onBack(){
        onBackPressed();
    }
    @BindView(R.id.act_title)
    TextView act_title;
    @OnClick(R.id.notification)
    void onNotification(){
        startActivity(new Intent(mContext,NotificationActivity.class));
    }
    @BindView(R.id.request_type)
    TextView request_type;
    @BindView(R.id.address)
    TextView address;
    @BindView(R.id.request_date)
    TextView request_date;
    @BindView(R.id.request_description)
    TextView request_description;
    @BindView(R.id.request_time)
    TextView request_time;
    @BindView(R.id.images)
    RecyclerView images;
    @BindView(R.id.send)
    Button send;
    String phone;
    String email;
    @BindView(R.id.mail)
    TextView mail;
    @BindView(R.id.call)
    TextView call;


    LinearLayoutManager linearLayoutManager;
    ArrayList<String> image = new ArrayList<>();
    ImagesAdapter imagesAdapter;
    OrderDetailsModel orderDetailsModel;

    String id;

    @Override
    protected void initializeComponents() {
        send.setVisibility(View.GONE);
        act_title.setText(getString(R.string.order_details));
        id = getIntent().getStringExtra("id");
        linearLayoutManager = new LinearLayoutManager(mContext,LinearLayoutManager.HORIZONTAL,false);
        imagesAdapter = new ImagesAdapter(mContext,image,R.layout.recycler_images);
        images.setLayoutManager(linearLayoutManager);
        images.setAdapter(imagesAdapter);
        getOrder();


    }
    @OnClick(R.id.location)
    void onLocation(){
        Intent intent = new Intent(mContext,LocationOnMapActivity.class);
        intent.putExtra("lat",orderDetailsModel.getLat());
        intent.putExtra("lng",orderDetailsModel.getLng());
        intent.putExtra("name",orderDetailsModel.getId()+"");
        startActivity(intent);
    }
    void SetData(OrderDetailsModel orderDetailsModel){
        request_type.setText(orderDetailsModel.getCategory());
        address.setText(orderDetailsModel.getLocation());
        request_date.setText(orderDetailsModel.getDate());
        request_time.setText(orderDetailsModel.getTime());
        request_description.setText(orderDetailsModel.getDetails());
        imagesAdapter.updateAll(orderDetailsModel.getImages());
        if (orderDetailsModel.getStatus().equals("client_accepted")){
            send.setVisibility(View.VISIBLE);
        }else {
            send.setVisibility(View.GONE);
        }
        phone = orderDetailsModel.getClient_phone();
        email = orderDetailsModel.getClient_email();

    }
    @OnClick(R.id.call)
    void onCall(){
        if (!phone.equals("")) {
            getLocationWithPermission(phone);
        } else {
            CommonUtil.makeToast(mContext, getString(R.string.no));
        }
    }
    @OnClick(R.id.mail)
    void onMail(){
        if (!email.equals("")) {
            sendEmail(email);
        } else {
            CommonUtil.makeToast(mContext, getString(R.string.no));
        }
    }
    void sendEmail(String email) {

        Log.i("Send email", "");
        String[] TO = {email};
        String[] CC = {""};
        Intent emailIntent = new Intent(Intent.ACTION_SEND);

        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
        emailIntent.putExtra(Intent.EXTRA_CC, CC);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Your subject");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "Email message goes here");

        try {
            startActivity(Intent.createChooser(emailIntent, "Send mail..."));
            finish();
            Log.i("Finished sending emal.", "");
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(getApplicationContext(), "There is no email client installed.", Toast.LENGTH_SHORT).show();
        }
    }
    public void getLocationWithPermission(String number) {
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!PermissionUtils.hasPermissions(OrderDetailsActivity.this, PermissionUtils.CALL_PHONE)) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(PermissionUtils.CALL_PHONE,
                            Constant.RequestPermission.REQUEST_CALL);
                }
            } else {
                callnumber(number);
            }
        } else {
            callnumber(number);
        }

    }

    @Override
    public void onRequestPermissionsResult(int permsRequestCode, String[] permissions, int[] grantResults) {
        switch (permsRequestCode) {
            case 800: {
                if (grantResults.length > 0) {
                    boolean Locationpermission = (grantResults[0] == PackageManager.PERMISSION_GRANTED);
                    callnumber(phone);
                    for (int i = 0; i < grantResults.length; i++) {
                    }
                } else {
                }
                return;
            }
        }
    }
    void callnumber(String number) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + number));
        startActivity(intent);
    }


    @Override
    protected int getLayoutResource() {
        return R.layout.activity_order_details;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }
    private void getOrder(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getOrder(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getApi_token(),id).enqueue(new Callback<OrderDetailsResponse>() {
            @Override
            public void onResponse(Call<OrderDetailsResponse> call, Response<OrderDetailsResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getValue().equals("1")){
                        orderDetailsModel = response.body().getData();
                        SetData(orderDetailsModel);
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<OrderDetailsResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    @OnClick(R.id.send)
    void onSend(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).finish(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getApi_token(),id).enqueue(
                new Callback<BaseResponse>() {
                    @Override
                    public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                        hideProgressDialog();
                        if (response.isSuccessful()){
                            if (response.body().getValue().equals("1")){
                                startActivity(new Intent(mContext,BackToHomeActivity.class));
                            }else {
                                CommonUtil.makeToast(mContext,response.body().getMsg());
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<BaseResponse> call, Throwable t) {
                        CommonUtil.handleException(mContext,t);
                        t.printStackTrace();
                        hideProgressDialog();


                    }
                }
        );
    }

}

