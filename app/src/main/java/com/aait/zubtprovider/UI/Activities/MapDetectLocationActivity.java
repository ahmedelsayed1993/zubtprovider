package com.aait.zubtprovider.UI.Activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.aait.zubtprovider.App.Constant;
import com.aait.zubtprovider.Base.ParentActivity;
import com.aait.zubtprovider.Gps.GPSTracker;
import com.aait.zubtprovider.Gps.GpsTrakerListener;
import com.aait.zubtprovider.R;
import com.aait.zubtprovider.Uitls.CommonUtil;
import com.aait.zubtprovider.Uitls.DialogUtil;
import com.aait.zubtprovider.Uitls.PermissionUtils;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;

public class MapDetectLocationActivity extends ParentActivity
        implements OnMapReadyCallback, GoogleMap.OnMapClickListener, GpsTrakerListener {

    @BindView(R.id.map)
    MapView mapView;



    GoogleMap googleMap;

    Marker myMarker;

    GPSTracker gps;

    public String mLang, mLat;

    private AlertDialog mAlertDialog;

    boolean startTracker = false;

    String mResult;
    Geocoder geocoder;
    String mResult1;
    Geocoder geocoder1;
    String mResult2;
    Geocoder geocoder2;
    @OnClick(R.id.act_back)
    void onBack(){
        onBackPressed();
    }
    @BindView(R.id.notification)
    ImageView notification;

    @Override
    protected void initializeComponents() {
      notification.setVisibility(View.GONE);

        mapView.onCreate(mSavedInstanceState);
        mapView.onResume();
        mapView.getMapAsync(this);

        try {
            MapsInitializer.initialize(this);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_map;
    }


    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        googleMap.setOnMapClickListener(this);
        getLocationWithPermission();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onMapClick(LatLng latLng) {
        Log.e("LatLng", latLng.toString());
        mLang = Double.toString(latLng.latitude);
        mLat = Double.toString(latLng.longitude);
        if (myMarker != null) {
            myMarker.remove();
            putMapMarker(latLng.latitude, latLng.longitude);
        } else {
            putMapMarker(latLng.latitude, latLng.longitude);
        }

            if (latLng.latitude != 0.0 && latLng.longitude != 0.0) {
                putMapMarker(latLng.latitude, latLng.longitude);
                mLat = String.valueOf(latLng.latitude);
                mLang = String.valueOf(latLng.longitude);
                List<Address> addresses;
                List<Address> addresses1;
                List<Address> addresses2;
                geocoder = new Geocoder(MapDetectLocationActivity.this, Locale.ENGLISH);
                geocoder1 = new Geocoder(MapDetectLocationActivity.this, new Locale("ar"));
                geocoder2 = new Geocoder(MapDetectLocationActivity.this, Locale.getDefault());
                try {
                    addresses = geocoder.getFromLocation(Double.parseDouble(mLat), Double.parseDouble(mLang), 1);
                    addresses1 = geocoder1.getFromLocation(Double.parseDouble(mLat), Double.parseDouble(mLang), 1);
                    addresses2 = geocoder2.getFromLocation(Double.parseDouble(mLat), Double.parseDouble(mLang), 1);
                    if (addresses.isEmpty()){
                        Toast.makeText(MapDetectLocationActivity.this, getResources().getString(R.string.detect_location), Toast.LENGTH_SHORT).show();
                    }
                    else{mResult = addresses.get(0).getAddressLine(0);
                       // CommonUtil.makeToast(mContext,mResult);
                    }
                    if (addresses1.isEmpty()){
                        Toast.makeText(MapDetectLocationActivity.this, getResources().getString(R.string.detect_location), Toast.LENGTH_SHORT).show();
                    }
                    else{mResult1 = addresses1.get(0).getAddressLine(0);
                       // CommonUtil.makeToast(mContext,mResult1);
                    }
                    if (addresses2.isEmpty()){
                        Toast.makeText(MapDetectLocationActivity.this, getResources().getString(R.string.detect_location), Toast.LENGTH_SHORT).show();
                    }
                    else{mResult2 = addresses.get(0).getAddressLine(0);
                      //  CommonUtil.makeToast(mContext,mResult2);
                    }

                } catch (IOException e) {}
                googleMap.clear();
                putMapMarker(latLng.latitude, latLng.longitude);
            }

    }

    @OnClick(R.id.btn_finish_location)
    void onDetectedSuccess() {
        Log.e("Location", "Lat:" + mLat + " Lng:" + mLang +"address"+mResult+"  "+mResult1+"  "+mResult2);
        if (mLang != null && mLat != null ) {
            Intent intentData = new Intent();
            intentData.putExtra(Constant.LocationConstant.LOCATION, mResult);
            intentData.putExtra("LOCATION", mResult1);
            intentData.putExtra("LOC", mResult2);
            intentData.putExtra(Constant.LocationConstant.LAT, mLat);
            intentData.putExtra(Constant.LocationConstant.LNG, mLang);
            setResult(RESULT_OK, intentData);
            finish();
        }
    }




    public void putMapMarker(Double lat, Double log) {
        Log.e("LatLng:", "Lat: " + lat + " Lng: " + log);
        // getLocationInfo("" + lat, "" + log, "ar");
        LatLng latLng = new LatLng(lat, log);
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
        MarkerOptions marker = new MarkerOptions().position(
                new LatLng(lat, log));
        marker.icon(BitmapDescriptorFactory
                .fromResource(R.mipmap.maps_and_flags));
        marker.title("موقعي");
        myMarker = googleMap.addMarker(marker);
    }

    @Override
    public void onTrackerSuccess(Double lat, Double log) {
        Log.e("Direction", "Direction Success");
        // dismiss traker dialog
        if (startTracker) {
            if (lat != 0.0 && log != 0.0) {
                hideProgressDialog();
                Log.e("LATLNG", "Lat:" + mLat + "  Lng:" + Double.toString(log));
                putMapMarker(lat, log);
            }
        }
    }

    @Override
    public void onStartTracker() {
        startTracker = true;
        //showProgressDialog(getString(R.string.detecting_location));
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e("Code", "requestCode: " + requestCode);
        switch (requestCode) {
            case Constant.RequestCode.GET_LOCATION: {
                if (resultCode == RESULT_OK) {
                    Log.e("Code", "request GPS Enabled True");
                    getCurrentLocation();
                }
            }
        }
    }


    @Override
    public void onRequestPermissionsResult(int permsRequestCode, String[] permissions, int[] grantResults) {
        switch (permsRequestCode) {
            case 800: {
                if (grantResults.length > 0) {
                    boolean Locationpermission = (grantResults[0] == PackageManager.PERMISSION_GRANTED);
                    Log.e("Permission", "All permission are granted");
                    getCurrentLocation();
                    for (int i = 0; i < grantResults.length; i++) {
                        Log.e("Permission", grantResults[0] + "");
                    }
                } else {
                    Log.e("Permission", "permission arn't granted");
                }
                return;
            }
        }
    }


    public void getLocationWithPermission() {
        gps = new GPSTracker(this, this);
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!PermissionUtils.hasPermissions(MapDetectLocationActivity.this, PermissionUtils.GPS_PERMISSION)) {
                CommonUtil.PrintLogE("Permission not granted");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(PermissionUtils.GPS_PERMISSION,
                            Constant.RequestPermission.REQUEST_GPS_LOCATION);
                    Log.e("GPS", "1");
                }
            } else {
                getCurrentLocation();
                Log.e("GPS", "2");
            }
        } else {
            Log.e("GPS", "3");
            getCurrentLocation();
        }

    }

    void getCurrentLocation() {
        gps.getLocation();
        if (!gps.canGetLocation()) {
            mAlertDialog = DialogUtil.showAlertDialog(MapDetectLocationActivity.this,
                    getString(R.string.gps_detecting), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(final DialogInterface dialogInterface, final int i) {
                            mAlertDialog.dismiss();
                            Intent intent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            startActivityForResult(intent, Constant.RequestCode.GPS_ENABLING);
                        }
                    });
        } else {
            if (gps.getLatitude() != 0.0 && gps.getLongitude() != 0.0) {
                putMapMarker(gps.getLatitude(), gps.getLongitude());
                mLat = String.valueOf(gps.getLatitude());
                mLang = String.valueOf(gps.getLongitude());
                List<Address> addresses;
                List<Address> addresses1;
                List<Address> addresses2;
                geocoder = new Geocoder(MapDetectLocationActivity.this, Locale.ENGLISH);
                geocoder1 = new Geocoder(MapDetectLocationActivity.this, new Locale("ar"));
                geocoder2 = new Geocoder(MapDetectLocationActivity.this, Locale.getDefault());
                try {
                    addresses = geocoder.getFromLocation(Double.parseDouble(mLat), Double.parseDouble(mLang), 1);
                    addresses1 = geocoder1.getFromLocation(Double.parseDouble(mLat), Double.parseDouble(mLang), 1);
                    addresses2 = geocoder2.getFromLocation(Double.parseDouble(mLat), Double.parseDouble(mLang), 1);
                    if (addresses.isEmpty()) {
                        Toast.makeText(MapDetectLocationActivity.this, getResources().getString(R.string.detect_location), Toast.LENGTH_SHORT).show();
                    } else {
                        mResult = addresses.get(0).getAddressLine(0);
                       // CommonUtil.makeToast(mContext, mResult);
                    }
                    if (addresses1.isEmpty()) {
                        Toast.makeText(MapDetectLocationActivity.this, getResources().getString(R.string.detect_location), Toast.LENGTH_SHORT).show();
                    } else {
                        mResult1 = addresses1.get(0).getAddressLine(0);
                        //CommonUtil.makeToast(mContext, mResult1);
                    }
                    if (addresses2.isEmpty()) {
                        Toast.makeText(MapDetectLocationActivity.this, getResources().getString(R.string.detect_location), Toast.LENGTH_SHORT).show();
                    } else {
                        mResult2 = addresses.get(0).getAddressLine(0);
                        //CommonUtil.makeToast(mContext, mResult2);
                    }

                } catch (IOException e) {
                }
                googleMap.clear();
                putMapMarker(Double.parseDouble(mLat), Double.parseDouble(mLang));
            }
        }
    }

    public static void startActivityForResult(AppCompatActivity appCompatActivity) {
        Intent intent = new Intent(appCompatActivity, MapDetectLocationActivity.class);
        appCompatActivity.startActivityForResult(intent, Constant.RequestCode.GET_LOCATION);
    }


    public static void startActivity(AppCompatActivity appCompatActivity) {
        Intent intent = new Intent(appCompatActivity, MapDetectLocationActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        appCompatActivity.startActivity(intent);
    }

}

