package com.aait.zubtprovider.UI.Activities;

/**
 * created by ahmed el_sayed  8/10/2019
 */

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.aait.zubtprovider.App.Constant;
import com.aait.zubtprovider.Base.ParentActivity;
import com.aait.zubtprovider.Listeners.OnItemClickListener;
import com.aait.zubtprovider.Models.CityModel;
import com.aait.zubtprovider.Models.CityResponse;
import com.aait.zubtprovider.Models.RegisterResponse;
import com.aait.zubtprovider.Network.RetroWeb;
import com.aait.zubtprovider.Network.ServiceApi;
import com.aait.zubtprovider.R;
import com.aait.zubtprovider.UI.Views.ListDialog;
import com.aait.zubtprovider.Uitls.CommonUtil;
import com.aait.zubtprovider.Uitls.PermissionUtils;
import com.aait.zubtprovider.Uitls.ProgressRequestBody;
import com.fxn.pix.Pix;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import gun0912.tedbottompicker.TedBottomPicker;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.aait.zubtprovider.App.Constant.RequestPermission.REQUEST_IMAGES;

public class RegisterActivity extends ParentActivity implements OnItemClickListener, ProgressRequestBody.UploadCallbacks {
    @BindView(R.id.user_name)
    EditText user_name;
    @BindView(R.id.phone)
    EditText phone;
    @BindView(R.id.email)
    EditText email;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.check_conditions)
    CheckBox check_conditions;
    @BindView(R.id.terms)
    TextView terms;
    @OnClick(R.id.login)
    void onLogin(){
        startActivity(new Intent(mContext,LoginActivity.class));
    }
    @OnClick(R.id.terms)
    void onTerms(){
        Intent intent = new Intent(mContext,TermsAndConditionsActivity.class);
        intent.putExtra("type","1");
        startActivity(intent);
    }
    int selected = 0;
    ArrayList<CityModel> cityModels = new ArrayList<>();
    ArrayList<CityModel> categoryModels = new ArrayList<>();
    ListDialog listDialog;
    CityModel city;
    CityModel category;
    @BindView(R.id.choose_category)
    TextView choose_category;
    @BindView(R.id.choose_city)
    TextView choose_city;
    ArrayList<Uri> ImageList = new ArrayList<>();

    String ImageBasePath = null;
    @BindView(R.id.id_image)
    TextView id_image;
    @BindView(R.id.request_location)
    TextView request_location;
    String mAdresse="", mLang=null, mLat = null, mAddress="",address = "";
    @OnClick(R.id.view)
    void onView(){

        password.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);

    }
    @Override
    protected void initializeComponents() {

    }
    @OnClick(R.id.choose_city)
    void onCity(){
        selected = 0;
        getCities();
    }
    @OnClick(R.id.choose_category)
    void onCategory(){
        selected = 1;
        getCategory();
    }
    @OnClick(R.id.request_location)
    void onLocation(){
        MapDetectLocationActivity.startActivityForResult((AppCompatActivity)mContext);
    }
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data != null) {
            if (requestCode == Constant.RequestCode.GET_LOCATION) {
                if (resultCode == RESULT_OK) {
                    mAdresse = data.getStringExtra(Constant.LocationConstant.LOCATION);
                    mAddress = data.getStringExtra("LOCATION");
                    address = data.getStringExtra("LOC");
                    mLang = data.getStringExtra(Constant.LocationConstant.LNG);
                    mLat = data.getStringExtra(Constant.LocationConstant.LAT);
                    CommonUtil.PrintLogE("Lat : " + mLat + " Lng : " + mLang + " Address : " + mAdresse+"  "+mAddress+"  "+address);
                    if (mLanguagePrefManager.getAppLanguage().equals("ar")) {
                        request_location.setText(mAddress);
                    }else if (mLanguagePrefManager.getAppLanguage().equals("en")){
                        request_location.setText(mAdresse);
                    }

                }
            }else if (requestCode == Constant.RequestPermission.REQUEST_IMAGES){
                ArrayList<String> returnValue = data.getStringArrayListExtra(Pix.IMAGE_RESULTS);

                ImageBasePath =returnValue.get(0);
                id_image.setText(ImageBasePath);
            }
        }
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_register;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }
    @OnClick(R.id.register)
    void onRegister(){
        if (CommonUtil.checkTextError((AppCompatActivity)mContext,user_name,getString(R.string.enter_user_name))||
        CommonUtil.checkTextError((AppCompatActivity)mContext,phone,getString(R.string.enter_phone))||
                CommonUtil.checkLength(phone,getString(R.string.phone_length),9)||
        CommonUtil.checkTextError((AppCompatActivity)mContext,email,getString(R.string.enter_email))||
                !CommonUtil.isEmailValid(email,getString(R.string.correct_email))||
        CommonUtil.checkTextError(request_location,getString(R.string.enter_location))||
        CommonUtil.checkTextError(id_image,getString(R.string.id_image))||
        CommonUtil.checkTextError(choose_city,getString(R.string.choose_city))||
        CommonUtil.checkTextError(choose_category,getString(R.string.choose_category))||
        CommonUtil.checkTextError((AppCompatActivity)mContext,password,getString(R.string.enter_password))||
                CommonUtil.checkLength(password,getString(R.string.password_length),6)
        ){
            return;

        }else {
            if (check_conditions.isChecked()){
                Register(ImageBasePath);
            }else {
                CommonUtil.makeToast(mContext,getString(R.string.check_terms));
            }
        }
    }

    private void Register(String path){
        showProgressDialog(getString(R.string.please_wait));
        MultipartBody.Part filePart = null;
        File ImageFile = new File(path);
        ProgressRequestBody fileBody = new ProgressRequestBody(ImageFile, RegisterActivity.this);
        filePart = MultipartBody.Part.createFormData("national_id", ImageFile.getName(), fileBody);
        RetroWeb.getClient().create(ServiceApi.class).register(mLanguagePrefManager.getAppLanguage(),user_name.getText().toString(),phone.getText().toString(),email.getText().toString(),password.getText().toString(),"provider",mLat,mLang,city.getId(),category.getId(),mAddress,mAdresse,filePart).enqueue(new Callback<RegisterResponse>() {
            @Override
            public void onResponse(Call<RegisterResponse> call, Response<RegisterResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getValue().equals("1")){
                        Intent intent = new Intent(mContext,ActivateAccountActivity.class);
                        intent.putExtra("token",response.body().getData().getApi_token());
                        intent.putExtra("code",response.body().getData().getCode());
                        startActivity(intent);
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<RegisterResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    private void getCities(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getCity(mLanguagePrefManager.getAppLanguage()).enqueue(new Callback<CityResponse>() {
            @Override
            public void onResponse(Call<CityResponse> call, Response<CityResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getValue().equals("1")){
                        cityModels = response.body().getData();
                        listDialog = new ListDialog(mContext,RegisterActivity.this,cityModels,getString(R.string.choose_city));
                        listDialog.show();
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<CityResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }

    private void getCategory(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getCategory(mLanguagePrefManager.getAppLanguage()).enqueue(new Callback<CityResponse>() {
            @Override
            public void onResponse(Call<CityResponse> call, Response<CityResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getValue().equals("1")){
                        categoryModels = response.body().getData();
                        listDialog = new ListDialog(mContext,RegisterActivity.this,categoryModels,getString(R.string.choose_category));
                        listDialog.show();
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<CityResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }

    @Override
    public void onItemClick(View view, int position) {
        listDialog.dismiss();
        if (view.getId()==R.id.tv_row_title){
            if (selected==0){
                city = cityModels.get(position);
                choose_city.setText(city.getValue());
            }else if (selected ==1){
                category = categoryModels.get(position);
                choose_category.setText(category.getValue());
            }
        }

    }
    @OnClick(R.id.id_image)
    void onImage(){
        getPickImageWithPermission();
    }
    public void getPickImageWithPermission() {
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext,PermissionUtils.IMAGE_PERMISSIONS))) {
                CommonUtil.PrintLogE("Permission not granted");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(PermissionUtils.IMAGE_PERMISSIONS,
                            REQUEST_IMAGES);
                }
            } else {
                Pix.start(this, REQUEST_IMAGES, 1);
                CommonUtil.PrintLogE("Permission is granted before");
            }
        } else {
            CommonUtil.PrintLogE("SDK minimum than 23");
            Pix.start(this, REQUEST_IMAGES, 1);
        }
    }

    void pickMultiImages() {
        TedBottomPicker bottomSheetDialogFragment = new TedBottomPicker.Builder(mContext)
                .setOnMultiImageSelectedListener(new TedBottomPicker.OnMultiImageSelectedListener() {
                    @Override
                    public void onImagesSelected(ArrayList<Uri> uriList) {
                        ImageList = uriList;
                        ImageBasePath = ImageList.get(0).getPath();
                        id_image.setText(ImageBasePath);

                    }
                })
                .setTitle("avatar")
                .setSelectMaxCount(1)
                .setSelectMinCount(1)
                .setPeekHeight(2600)
                .showTitle(false)
                .setCompleteButtonText(R.string.choose)
                .setEmptySelectionText(R.string.no_image)
                .create();
        bottomSheetDialogFragment.show(getSupportFragmentManager());
    }

    @Override
    public void onProgressUpdate(int percentage) {

    }

    @Override
    public void onError() {

    }

    @Override
    public void onFinish() {

    }
}
