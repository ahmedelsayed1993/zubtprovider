package com.aait.zubtprovider.UI.Activities;
/**
 * created by ahmed el_sayed  8/10/2019
 */

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;

import com.aait.zubtprovider.Base.ParentActivity;
import com.aait.zubtprovider.Models.BaseResponse;
import com.aait.zubtprovider.Network.RetroWeb;
import com.aait.zubtprovider.Network.ServiceApi;
import com.aait.zubtprovider.R;
import com.aait.zubtprovider.Uitls.CommonUtil;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RecoverPasswordActivity extends ParentActivity {
    @BindView(R.id.code)
    EditText code;
    @BindView(R.id.new_pass)
    EditText new_pass;
    @BindView(R.id.confirm_new_pass)
    EditText confirm_new_pass;
    String token , Code;
    @OnClick(R.id.send)
    void onSend(){
        if (CommonUtil.checkTextError((AppCompatActivity)mContext,code,getString(R.string.enter_activation_code))||
        CommonUtil.checkTextError((AppCompatActivity)mContext,new_pass,getString(R.string.enter_new_password))||
        CommonUtil.checkTextError((AppCompatActivity)mContext,confirm_new_pass,getString(R.string.confirm_new_password))){
            return;
        }else {
            if (new_pass.getText().toString().equals(confirm_new_pass.getText().toString())){
                Recover();
            }else {
                CommonUtil.makeToast(mContext,getString(R.string.password_not_match));
            }
        }
    }
    @Override
    protected void initializeComponents() {
        token = getIntent().getStringExtra("token");
        Code = getIntent().getStringExtra("code");

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_recover_password;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }
    private void Recover(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).resetPassword(mLanguagePrefManager.getAppLanguage(),token,new_pass.getText().toString(),code.getText().toString()).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getValue().equals("1")){
                        startActivity(new Intent(mContext,LoginActivity.class));
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
}
