package com.aait.zubtprovider.UI.Views;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.view.Gravity;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;

import com.aait.zubtprovider.Models.BaseResponse;
import com.aait.zubtprovider.Network.RetroWeb;
import com.aait.zubtprovider.Network.ServiceApi;
import com.aait.zubtprovider.Pereferences.LanguagePrefManager;
import com.aait.zubtprovider.Pereferences.SharedPrefManager;
import com.aait.zubtprovider.R;
import com.aait.zubtprovider.Uitls.CommonUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangePasswordDiolg extends Dialog {
    public ChangePasswordDiolg(@NonNull Context context) {
        super(context);
        this.mContext = context;
    }
    Context mContext;
    SharedPrefManager sharedPreferences;
    LanguagePrefManager languagePrefManager;
    @BindView(R.id.old_pass)
    EditText old_pass;
    @BindView(R.id.new_pass)
    EditText new_pass;
    @BindView(R.id.confirm_new_pass)
    EditText confirm_new_pass;
    @OnClick(R.id.save)
    void onSave(){
        if (CommonUtil.checkTextError((AppCompatActivity)mContext,old_pass,mContext.getString(R.string.enter_old_password))||
        CommonUtil.checkTextError((AppCompatActivity)mContext,new_pass,mContext.getString(R.string.enter_new_password))||
                CommonUtil.checkLength(new_pass,mContext.getString(R.string.password_length),6)||
        CommonUtil.checkTextError((AppCompatActivity)mContext,confirm_new_pass,mContext.getString(R.string.confirm_new_password))){
            return;
        }else {
            if (!new_pass.getText().toString().equals(confirm_new_pass.getText().toString())){
                CommonUtil.makeToast(mContext,mContext.getString(R.string.password_not_match));
            }else {
                changePassword();
            }
        }
    }


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dailog_change_pass);
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        getWindow().setGravity(Gravity.CENTER);
        setCancelable(true);
        ButterKnife.bind(this);
        sharedPreferences = new SharedPrefManager(mContext);
        languagePrefManager = new LanguagePrefManager(mContext);
        initializeComponents();
    }
    private void initializeComponents() {

    }
    @OnClick(R.id.view)
    void onView(){

        old_pass.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);

    }
    @OnClick(R.id.view1)
    void onView1(){

        new_pass.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);

    }

    @OnClick(R.id.view2)
    void onView2(){

        confirm_new_pass.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);

    }
    private void changePassword(){
        RetroWeb.getClient().create(ServiceApi.class).changePass(languagePrefManager.getAppLanguage(),sharedPreferences.getUserData().getApi_token(),old_pass.getText().toString(),new_pass.getText().toString()).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (response.isSuccessful()){
                    if (response.body().getValue().equals("1")){
                        CommonUtil.makeToast(mContext,mContext.getString(R.string.password_changed_successfully));
                        ChangePasswordDiolg.this.cancel();
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();

            }
        });
    }
}
