package com.aait.zubtprovider.UI.Activities;

import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aait.zubtprovider.Base.ParentActivity;
import com.aait.zubtprovider.Models.BankResponse;
import com.aait.zubtprovider.Models.Bank_model;
import com.aait.zubtprovider.Models.OrderModel;
import com.aait.zubtprovider.Network.RetroWeb;
import com.aait.zubtprovider.Network.ServiceApi;
import com.aait.zubtprovider.R;
import com.aait.zubtprovider.UI.Adapters.BanksAdapter;
import com.aait.zubtprovider.UI.Adapters.OrdersAdapter;
import com.aait.zubtprovider.Uitls.CommonUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BankAccountsActivity extends ParentActivity {
    @BindView(R.id.rv_recycle)
    RecyclerView rvRecycle;
    @BindView(R.id.lay_progress)
    RelativeLayout layProgress;
    @BindView(R.id.lay_no_internet)
    RelativeLayout layNoInternet;
    @BindView(R.id.lay_no_item)
    RelativeLayout layNoItem;
    @BindView(R.id.tv_no_content)
    TextView tvNoContent;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefresh;
    LinearLayoutManager linearLayoutManager;
    BanksAdapter banksAdapter;
    ArrayList<Bank_model> bank_models=new ArrayList<>();
    @OnClick(R.id.act_back)
    void onBack(){
        onBackPressed();
    }
    @OnClick(R.id.notification)
    void onNotification(){
        startActivity(new Intent(mContext,NotificationActivity.class));
    }
    @BindView(R.id.act_title)
    TextView act_title;
    @Override
    protected void initializeComponents() {
        act_title.setText(getString(R.string.financial_accounts));
        linearLayoutManager = new LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false);
        banksAdapter = new BanksAdapter(mContext,bank_models,R.layout.recycler_bank_account);
        rvRecycle.setLayoutManager(linearLayoutManager);
        rvRecycle.setAdapter(banksAdapter);
        swipeRefresh.setColorSchemeResources(R.color.colorWhite, R.color.colorPrimary, R.color.colorPrimaryDark);
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
               getBanks();
            }
        });
        getBanks();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_bank_accounts;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }
private void getBanks(){
    layProgress.setVisibility(View.VISIBLE);
    layNoInternet.setVisibility(View.GONE);
    layNoItem.setVisibility(View.GONE);
    RetroWeb.getClient().create(ServiceApi.class).getBanks(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getApi_token()).enqueue(new Callback<BankResponse>() {
        @Override
        public void onResponse(Call<BankResponse> call, Response<BankResponse> response) {
            layProgress.setVisibility(View.GONE);
            swipeRefresh.setRefreshing(false);
            if (response.isSuccessful()){
                if (response.body().getValue().equals("1")){
                    if (response.body().getData().isEmpty()){
                        layNoItem.setVisibility(View.VISIBLE);
                        layNoInternet.setVisibility(View.GONE);
                        tvNoContent.setText(R.string.content_not_found_you_can_still_search_the_app_freely);
                    }else {
                        banksAdapter.updateAll(response.body().getData());
                    }
                }else {
                    CommonUtil.makeToast(mContext,response.body().getMsg());
                }
            }
        }

        @Override
        public void onFailure(Call<BankResponse> call, Throwable t) {
            CommonUtil.handleException(mContext, t);
            t.printStackTrace();
            layNoInternet.setVisibility(View.VISIBLE);
            layNoItem.setVisibility(View.GONE);
            layProgress.setVisibility(View.GONE);
            swipeRefresh.setRefreshing(false);
        }
    });
}
@OnClick(R.id.confirm)
    void onConfirm(){
        startActivity(new Intent(mContext,PayActivity.class));
}
}
