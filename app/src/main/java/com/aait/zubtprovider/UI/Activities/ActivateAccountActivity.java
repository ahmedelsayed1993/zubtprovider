package com.aait.zubtprovider.UI.Activities;
/**
 * created by ahmed el_sayed  8/10/2019
 */

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;

import com.aait.zubtprovider.Base.ParentActivity;
import com.aait.zubtprovider.Models.BaseResponse;
import com.aait.zubtprovider.Network.RetroWeb;
import com.aait.zubtprovider.Network.ServiceApi;
import com.aait.zubtprovider.R;
import com.aait.zubtprovider.Uitls.CommonUtil;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivateAccountActivity extends ParentActivity {
    String token , code;
    @BindView(R.id.code)
    EditText Code;
    @OnClick(R.id.confirm)
    void onConfirm(){
        if (CommonUtil.checkTextError((AppCompatActivity)mContext,Code,getString(R.string.enter_activation_code))){
            return;
        }else {
            Activate();
        }
    }
    @Override
    protected void initializeComponents() {
        token = getIntent().getStringExtra("token");
        code = getIntent().getStringExtra("code");

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_activate_account;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }

    private void Activate(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).Activate(mLanguagePrefManager.getAppLanguage(),token,Code.getText().toString()).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getValue().equals("1")){
                        CommonUtil.makeToast(mContext,getString(R.string.your_account_activated));
                        startActivity(new Intent(mContext,LoginActivity.class));
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
}
