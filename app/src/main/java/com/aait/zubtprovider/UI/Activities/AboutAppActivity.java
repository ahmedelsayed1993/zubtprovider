package com.aait.zubtprovider.UI.Activities;

import android.content.Intent;
import android.widget.TextView;

import com.aait.zubtprovider.Base.ParentActivity;
import com.aait.zubtprovider.Models.TermsResponse;
import com.aait.zubtprovider.Network.RetroWeb;
import com.aait.zubtprovider.Network.ServiceApi;
import com.aait.zubtprovider.R;
import com.aait.zubtprovider.Uitls.CommonUtil;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AboutAppActivity extends ParentActivity {
    @OnClick(R.id.act_back)
    void onBack(){
        onBackPressed();
    }
    @BindView(R.id.act_title)
    TextView act_title;
    @OnClick(R.id.notification)
    void onNotification(){
        startActivity(new Intent(mContext,NotificationActivity.class));
    }
    @BindView(R.id.terms)
    TextView terms;
    @Override
    protected void initializeComponents() {
        act_title.setText(getString(R.string.about_app));
        getTerms();


    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_terms_and_conditions;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }
    private void getTerms(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getTerms(mLanguagePrefManager.getAppLanguage()).enqueue(new Callback<TermsResponse>() {
            @Override
            public void onResponse(Call<TermsResponse> call, Response<TermsResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getValue().equals("1")){
                        terms.setText(response.body().getData().getAbout());
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<TermsResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
}
