package com.aait.zubtprovider.UI.Activities;

import android.content.Intent;

import com.aait.zubtprovider.Base.ParentActivity;
import com.aait.zubtprovider.R;

import butterknife.OnClick;

public class BackToHomeActivity extends ParentActivity {
    @Override
    protected void initializeComponents() {

    }
    @OnClick(R.id.back_to_home)
    void onBack(){
        startActivity(new Intent(mContext,MainActivity.class));
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_back_to_home;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }
}
