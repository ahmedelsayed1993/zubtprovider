package com.aait.zubtprovider.UI.Activities;

import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aait.zubtprovider.Base.ParentActivity;
import com.aait.zubtprovider.Listeners.OnItemClickListener;
import com.aait.zubtprovider.Listeners.PaginationScrollListener;
import com.aait.zubtprovider.Models.QuestionModel;
import com.aait.zubtprovider.Models.QuestionResponse;
import com.aait.zubtprovider.Network.RetroWeb;
import com.aait.zubtprovider.Network.ServiceApi;
import com.aait.zubtprovider.R;
import com.aait.zubtprovider.UI.Adapters.QuestionsAdapter;
import com.aait.zubtprovider.Uitls.CommonUtil;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RepeatedQuestionActivity extends ParentActivity implements OnItemClickListener {
    @OnClick(R.id.act_back)
    void onBack(){
        onBackPressed();
    }
    @BindView(R.id.act_title)
    TextView act_title;
    @BindView(R.id.rv_recycle)
    RecyclerView rvRecycle;
    @BindView(R.id.lay_progress)
    RelativeLayout layProgress;
    @BindView(R.id.lay_no_internet)
    RelativeLayout layNoInternet;
    @BindView(R.id.lay_no_item)
    RelativeLayout layNoItem;
    @BindView(R.id.tv_no_content)
    TextView tvNoContent;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefresh;
    LinearLayoutManager linearLayoutManager;
    @OnClick(R.id.notification)
    void onNotification(){
        startActivity(new Intent(mContext,NotificationActivity.class));
    }

    ArrayList<QuestionModel> questionModels = new ArrayList<>();
    QuestionsAdapter questionsAdapter;

    // Indicates if footer ProgressBar is shown (i.e. next page is loading)
    private boolean isLoading = false;

    // If current page is the last page (Pagination will stop after this page load)
    private boolean isLastPage = false;

    // total no. of pages to load. Initial load is page 0, after which 2 more pages will load.
    private int TOTAL_PAGES;

    // indicates the current page which Pagination is fetching.
    private int currentPage = 1;
    @Override
    protected void initializeComponents() {

        act_title.setText(getString(R.string.repeated_questions));
        linearLayoutManager = new LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false);
        questionsAdapter = new QuestionsAdapter(mContext,questionModels,R.layout.recycler_question);
        questionsAdapter.setOnItemClickListener(this);
        rvRecycle.setLayoutManager(linearLayoutManager);
        rvRecycle.setAdapter(questionsAdapter);
        rvRecycle.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage += 1;
                getQuestions();
            }

            @Override
            public int getTotalPageCount() {
                return 0;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });

        swipeRefresh.setColorSchemeResources(R.color.colorPrimary, R.color.colorPrimaryDark, R.color.colorWhite);
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                currentPage = 1;
                questionModels.clear();
                isLastPage = false;
                isLoading = false;
                getQuestions();
            }
        });

    }
    @Override
    protected void onResume() {
        super.onResume();
        currentPage = 1;
        questionModels.clear();
        isLastPage = false;
        isLoading = false;
        getQuestions();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_reapted_questions;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }
    private void getQuestions(){
        layProgress.setVisibility(View.VISIBLE);
        layNoInternet.setVisibility(View.GONE);
        layNoItem.setVisibility(View.GONE);
        RetroWeb.getClient().create(ServiceApi.class).getQuestions(mLanguagePrefManager.getAppLanguage(),currentPage).enqueue(new Callback<QuestionResponse>() {
            @Override
            public void onResponse(Call<QuestionResponse> call, Response<QuestionResponse> response) {
                layProgress.setVisibility(View.GONE);
                swipeRefresh.setRefreshing(false);
                if (response.isSuccessful()){
                    if (response.body().getValue().equals("1")){
                        TOTAL_PAGES = response.body().getData().getPagination().getTotal_pages();
                        if (currentPage == 1 &&response.body().getData().getQuestions().isEmpty()){
                            layNoItem.setVisibility(View.VISIBLE);
                            layNoInternet.setVisibility(View.GONE);
                            tvNoContent.setText(R.string.content_not_found_you_can_still_search_the_app_freely);
                        }
                        if (currentPage < TOTAL_PAGES) {
                            if (response.body().getData().getQuestions().isEmpty()) {
                                questionsAdapter.updateAll( response.body().getData().getQuestions());
                            } else {
                                questionsAdapter.InsertAll( response.body().getData().getQuestions());
                            }
                            isLoading = false;
                        }else if (currentPage == TOTAL_PAGES){
                            if (response.body().getData().getQuestions().isEmpty()) {
                                questionsAdapter.updateAll( response.body().getData().getQuestions());
                                isLastPage = true;
                            } else {
                                questionsAdapter.InsertAll( response.body().getData().getQuestions());
                            }
                        }
                        else {
                            isLastPage = true;
                        }

                    }
                }
            }

            @Override
            public void onFailure(Call<QuestionResponse> call, Throwable t) {
                CommonUtil.handleException(mContext, t);
                t.printStackTrace();
                layNoInternet.setVisibility(View.VISIBLE);
                layNoItem.setVisibility(View.GONE);
                layProgress.setVisibility(View.GONE);
                swipeRefresh.setRefreshing(false);
            }
        });
    }


    @Override
    public void onItemClick(View view, int position) {

    }
}
