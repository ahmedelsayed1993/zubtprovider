package com.aait.zubtprovider.UI.Activities;

import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.aait.zubtprovider.Base.ParentActivity;
import com.aait.zubtprovider.Models.ProviderDetialsResponse;
import com.aait.zubtprovider.Models.ReviewsModels;
import com.aait.zubtprovider.Network.RetroWeb;
import com.aait.zubtprovider.Network.ServiceApi;
import com.aait.zubtprovider.R;
import com.aait.zubtprovider.UI.Adapters.ReviewsAdapter;
import com.aait.zubtprovider.Uitls.CommonUtil;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProviderDetailsActivity extends ParentActivity {
    String id;
    @BindView(R.id.image)
    ImageView image;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.rate)
    TextView rate;
    @BindView(R.id.rating)
    RatingBar rating;
    @BindView(R.id.city)
    TextView city;
    @BindView(R.id.work_times)
    TextView work_times;
    @BindView(R.id.description)
    TextView description;

    @BindView(R.id.rv_recycle)
    RecyclerView rv_recycle;

    @OnClick(R.id.act_back)
    void onBack() {
        onBackPressed();
    }

    @OnClick(R.id.notification)
    void onNotification() {
        startActivity(new Intent(mContext, NotificationActivity.class));
    }


    @BindView(R.id.act_title)
    TextView act_title;
    LinearLayoutManager linearLayoutManager;
    ArrayList<ReviewsModels> reviewsModels = new ArrayList<>();
    ReviewsAdapter reviewsAdapter;

    @Override
    protected void initializeComponents() {
        act_title.setText(getString(R.string.profile));
        id = getIntent().getStringExtra("id");
        linearLayoutManager = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        reviewsAdapter = new ReviewsAdapter(mContext, reviewsModels, R.layout.recycler_reviews);
        rv_recycle.setLayoutManager(linearLayoutManager);
        rv_recycle.setAdapter(reviewsAdapter);
        if (mSharedPrefManager.getLoginStatus()) {
            getProvider(mSharedPrefManager.getUserData().getApi_token());
        } else {
            getProvider(null);
        }

    }



    @OnClick(R.id.request)
    void onRequest() {
       startActivity(new Intent(mContext,ProfileActivity.class));
       ProviderDetailsActivity.this.finish();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_provider_details;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }

    private void getProvider(String token) {
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getProvider(mLanguagePrefManager.getAppLanguage(), token, mSharedPrefManager.getUserData().getId()+"").enqueue(new Callback<ProviderDetialsResponse>() {
            @Override
            public void onResponse(Call<ProviderDetialsResponse> call, Response<ProviderDetialsResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()) {
                    if (response.body().getValue().equals("1")) {
                        Glide.with(mContext).load(response.body().getData().getAvatar()).apply(new RequestOptions().placeholder(R.mipmap.untitled_6).fitCenter()).into(image);
                        name.setText(response.body().getData().getName());

                        rate.setText(response.body().getData().getRate() + "");
                        rating.setRating(response.body().getData().getRate());
                        city.setText(response.body().getData().getLocation());
                        if (mLanguagePrefManager.getAppLanguage().equals("ar")){
                            work_times.setText(response.body().getData().getWork_to()+" : "+response.body().getData().getWork_from());

                        }
                        else {
                            work_times.setText(response.body().getData().getWork_from()+" : "+response.body().getData().getWork_to());

                        }
                        description.setText(response.body().getData().getDetails());

                        if (response.body().getData().getReviews().isEmpty()) {
                            rv_recycle.setVisibility(View.GONE);
                        } else {
                            rv_recycle.setVisibility(View.VISIBLE);
                            reviewsAdapter.updateAll(response.body().getData().getReviews());
                        }
                    } else {
                        CommonUtil.makeToast(mContext, response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<ProviderDetialsResponse> call, Throwable t) {
                CommonUtil.handleException(mContext, t);
                t.printStackTrace();
                hideProgressDialog();
            }
        });
    }
}