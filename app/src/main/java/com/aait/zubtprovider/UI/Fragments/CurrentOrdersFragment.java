package com.aait.zubtprovider.UI.Fragments;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aait.zubtprovider.Base.BaseFragment;
import com.aait.zubtprovider.Listeners.PaginationScrollListener;
import com.aait.zubtprovider.Models.OrderModel;
import com.aait.zubtprovider.Models.OrdersResponse;
import com.aait.zubtprovider.Network.RetroWeb;
import com.aait.zubtprovider.Network.ServiceApi;
import com.aait.zubtprovider.R;
import com.aait.zubtprovider.UI.Adapters.OrdersAdapter;
import com.aait.zubtprovider.Uitls.CommonUtil;

import java.util.ArrayList;

import butterknife.BindView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CurrentOrdersFragment extends BaseFragment {
    @BindView(R.id.rv_recycle)
    RecyclerView rvRecycle;
    @BindView(R.id.lay_progress)
    RelativeLayout layProgress;
    @BindView(R.id.lay_no_internet)
    RelativeLayout layNoInternet;
    @BindView(R.id.lay_no_item)
    RelativeLayout layNoItem;
    @BindView(R.id.tv_no_content)
    TextView tvNoContent;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefresh;
    LinearLayoutManager linearLayoutManager;
    OrdersAdapter ordersAdapter;
    ArrayList<OrderModel> orderModels=new ArrayList<>();
    // Indicates if footer ProgressBar is shown (i.e. next page is loading)
    private boolean isLoading = false;

    // If current page is the last page (Pagination will stop after this page load)
    private boolean isLastPage = false;

    // total no. of pages to load. Initial load is page 0, after which 2 more pages will load.
    private int TOTAL_PAGES;

    // indicates the current page which Pagination is fetching.
    private int currentPage = 1;
    @Override
    protected int getLayoutResource() {
        return R.layout.app_recycle;
    }

    @Override
    protected void initializeComponents(View view) {
        linearLayoutManager = new LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false);
        ordersAdapter = new OrdersAdapter(mContext,orderModels,R.layout.recycler_order);
        rvRecycle.setLayoutManager(linearLayoutManager);
        rvRecycle.setAdapter(ordersAdapter);
        rvRecycle.addOnScrollListener(new PaginationScrollListener(linearLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                currentPage += 1;
                gethome();
            }

            @Override
            public int getTotalPageCount() {
                return 0;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });


        swipeRefresh.setColorSchemeResources(R.color.colorPrimary, R.color.colorPrimaryDark, R.color.colorWhite);
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                currentPage = 1;
                orderModels.clear();
                isLastPage = false;
                isLoading = false;
                gethome();
            }
        });

    }
    @Override
    public void onResume() {
        super.onResume();
        currentPage = 1;
        orderModels.clear();
        isLastPage = false;
        isLoading = false;
        gethome();
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }
    private void gethome(){
        layProgress.setVisibility(View.VISIBLE);
        layNoInternet.setVisibility(View.GONE);
        layNoItem.setVisibility(View.GONE);
        RetroWeb.getClient().create(ServiceApi.class).getOrders(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getApi_token(),"current",currentPage).enqueue(new Callback<OrdersResponse>() {
            @Override
            public void onResponse(Call<OrdersResponse> call, Response<OrdersResponse> response) {
                layProgress.setVisibility(View.GONE);
                swipeRefresh.setRefreshing(false);
                if (response.isSuccessful()){
                    if (response.body().getValue().equals("1")){
                        TOTAL_PAGES = response.body().getData().getPagination().getTotal_pages();
                        if (currentPage == 1 &&response.body().getData().getOrders().isEmpty()){
                            layNoItem.setVisibility(View.VISIBLE);
                            layNoInternet.setVisibility(View.GONE);
                            tvNoContent.setText(R.string.no_oders);
                        }
                        if (currentPage < TOTAL_PAGES) {
                            if (response.body().getData().getOrders().isEmpty()) {
                                ordersAdapter.updateAll( response.body().getData().getOrders());
                            } else {
                                ordersAdapter.InsertAll( response.body().getData().getOrders());
                            }
                            isLoading = false;
                        }else if (currentPage == TOTAL_PAGES){
                            if (response.body().getData().getOrders().isEmpty()) {
                                ordersAdapter.updateAll( response.body().getData().getOrders());
                                isLastPage = true;
                            } else {
                                ordersAdapter.InsertAll( response.body().getData().getOrders());
                            }
                        }
                        else {
                            isLastPage = true;
                        }

                    }
                }
            }

            @Override
            public void onFailure(Call<OrdersResponse> call, Throwable t) {
                CommonUtil.handleException(mContext, t);
                t.printStackTrace();
                layNoInternet.setVisibility(View.VISIBLE);
                layNoItem.setVisibility(View.GONE);
                layProgress.setVisibility(View.GONE);
                swipeRefresh.setRefreshing(false);
            }
        });
    }
}
