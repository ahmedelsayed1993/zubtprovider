package com.aait.zubtprovider.UI.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aait.zubtprovider.Base.ParentRecyclerAdapter;
import com.aait.zubtprovider.Base.ParentRecyclerViewHolder;
import com.aait.zubtprovider.Models.QuestionModel;
import com.aait.zubtprovider.R;

import java.util.List;

import butterknife.BindView;

public class QuestionsAdapter extends ParentRecyclerAdapter<QuestionModel> {
    int count = 0;
    public QuestionsAdapter(Context context, List<QuestionModel> data, int layoutId) {
        super(context, data, layoutId);
    }

    @NonNull
    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(mcontext).inflate(layoutId, viewGroup, false);
        QuestionsAdapter.ViewHolder holder = new QuestionsAdapter.ViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ParentRecyclerViewHolder parentRecyclerViewHolder, final int i) {
        final QuestionsAdapter.ViewHolder viewHolder = (QuestionsAdapter.ViewHolder) parentRecyclerViewHolder;
        final QuestionModel categoryModel = data.get(i);
        viewHolder.question.setText(categoryModel.getQuestion());
        viewHolder.answer.setText(categoryModel.getAnswer());
        viewHolder.show.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                count++;
                itemClickListener.onItemClick(v,i);
                if (count%2==1){
                    viewHolder.answer.setVisibility(View.VISIBLE);
                    viewHolder.show.setImageDrawable(mcontext.getResources().getDrawable(R.mipmap.arrrow));
                    viewHolder.question.setBackgroundColor(mcontext.getResources().getColor(R.color.colorPrimaryDark));
                    viewHolder.lay.setBackgroundColor(mcontext.getResources().getColor(R.color.colorPrimaryDark));
                    viewHolder.question.setTextColor(mcontext.getResources().getColor(R.color.colorWhite));

                }else {
                    viewHolder.answer.setVisibility(View.GONE);
                    viewHolder.show.setImageDrawable(mcontext.getResources().getDrawable(R.mipmap.arroww));
                    viewHolder.question.setBackgroundColor(mcontext.getResources().getColor(R.color.colorWhite));
                    viewHolder.lay.setBackgroundColor(mcontext.getResources().getColor(R.color.colorWhite));
                    viewHolder.question.setTextColor(mcontext.getResources().getColor(R.color.colorPrimary));
                }

            }
        });
    }
    public class ViewHolder extends ParentRecyclerViewHolder {

        @BindView(R.id.question)
        TextView question;

        @BindView(R.id.answer)
        TextView answer;
        @BindView(R.id.show)
        ImageView show;
        @BindView(R.id.lay)
        LinearLayout lay;



        ViewHolder(View itemView) {
            super(itemView);
        }

    }
}
