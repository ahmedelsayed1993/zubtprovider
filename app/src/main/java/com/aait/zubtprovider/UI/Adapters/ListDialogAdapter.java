package com.aait.zubtprovider.UI.Adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.aait.zubtprovider.Base.ParentRecyclerAdapter;
import com.aait.zubtprovider.Base.ParentRecyclerViewHolder;
import com.aait.zubtprovider.Models.CityModel;
import com.aait.zubtprovider.R;

import java.util.List;

import butterknife.BindView;

public class ListDialogAdapter extends ParentRecyclerAdapter<CityModel> {

    public ListDialogAdapter(final Context context, final List<CityModel> data) {
        super(context, data);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        ParentRecyclerViewHolder viewHolder = null;
        View viewItem = inflater.inflate(R.layout.recycler_dialog_list_row, parent, false);
        viewHolder = new ListAdapter(viewItem);

        return viewHolder;
    }


    @Override
    public void onBindViewHolder(final ParentRecyclerViewHolder holder, final int position) {
        ListAdapter listAdapter = (ListAdapter) holder;
        CityModel listModel = data.get(position);
        listAdapter.tv_row_title.setText(listModel.getValue());
        if (position % 2 == 0) {
            listAdapter.tv_row_title.setBackgroundColor(ContextCompat.getColor(mcontext, R.color.colorWhite));
        } else {
            listAdapter.tv_row_title.setBackgroundColor(ContextCompat.getColor(mcontext, R.color.colorOffWhite));

        }

        listAdapter.tv_row_title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                itemClickListener.onItemClick(view, position);
            }
        });
    }


    protected class ListAdapter extends ParentRecyclerViewHolder {

        @BindView(R.id.tv_row_title)
        TextView tv_row_title;

        public ListAdapter(View itemView) {
            super(itemView);
            setClickableRootView(itemView);
        }
    }
}

