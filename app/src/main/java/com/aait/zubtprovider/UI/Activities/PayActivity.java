package com.aait.zubtprovider.UI.Activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.aait.zubtprovider.App.Constant;
import com.aait.zubtprovider.Base.ParentActivity;
import com.aait.zubtprovider.Models.BaseResponse;
import com.aait.zubtprovider.Network.RetroWeb;
import com.aait.zubtprovider.Network.ServiceApi;
import com.aait.zubtprovider.R;
import com.aait.zubtprovider.Uitls.CommonUtil;
import com.aait.zubtprovider.Uitls.PermissionUtils;
import com.aait.zubtprovider.Uitls.ProgressRequestBody;
import com.fxn.pix.Pix;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import gun0912.tedbottompicker.TedBottomPicker;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.aait.zubtprovider.App.Constant.RequestPermission.REQUEST_IMAGES;

public class PayActivity extends ParentActivity implements ProgressRequestBody.UploadCallbacks{
    @BindView(R.id.act_title)
    TextView act_title;
    @OnClick(R.id.act_back)
    void onBack(){
        onBackPressed();
    }
    @OnClick(R.id.notification)
    void onNotification(){
        startActivity(new Intent(mContext,NotificationActivity.class));
    }
    @BindView(R.id.account_owner)
    EditText account_owner;
    @BindView(R.id.bank_name)
    EditText bank_name;
    @BindView(R.id.account_number)
    EditText account_number;
    @BindView(R.id.amount)
    EditText amount;
    @BindView(R.id.add_image)
    ImageView add_image;
    ArrayList<Uri> ImageList = new ArrayList<>();

    String ImageBasePath = null;
    @Override
    protected void initializeComponents() {
        act_title.setText(getString(R.string.payment_data));

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_pay;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }
    @OnClick(R.id.add_image)
    void onImage(){
        getPickImageWithPermission();
    }

    @OnClick(R.id.send)
    void onSend(){
        if (CommonUtil.checkTextError((AppCompatActivity)mContext,account_owner,getString(R.string.enter_account_owner))||
        CommonUtil.checkTextError((AppCompatActivity)mContext,bank_name,getString(R.string.enter_bank_name))||
        CommonUtil.checkTextError((AppCompatActivity)mContext,account_number,getString(R.string.enter_account_number))||
        CommonUtil.checkTextError((AppCompatActivity)mContext,amount,getString(R.string.enter_amount))){
            return;
        }else {
            if (ImageBasePath==null){
                CommonUtil.makeToast(mContext,getString(R.string.add_image));
            }else {
                Pay(ImageBasePath);
            }
        }
    }
    public void getPickImageWithPermission() {
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext,PermissionUtils.IMAGE_PERMISSIONS))) {
                CommonUtil.PrintLogE("Permission not granted");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(PermissionUtils.IMAGE_PERMISSIONS,
                            REQUEST_IMAGES);
                }
            } else {
                Pix.start(this, REQUEST_IMAGES, 1);
                CommonUtil.PrintLogE("Permission is granted before");
            }
        } else {
            CommonUtil.PrintLogE("SDK minimum than 23");
            Pix.start(this, REQUEST_IMAGES, 1);
        }
    }

    void pickMultiImages() {
        TedBottomPicker bottomSheetDialogFragment = new TedBottomPicker.Builder(mContext)
                .setOnMultiImageSelectedListener(new TedBottomPicker.OnMultiImageSelectedListener() {
                    @Override
                    public void onImagesSelected(ArrayList<Uri> uriList) {
                        ImageList = uriList;
                        ImageBasePath = ImageList.get(0).getPath();
                        add_image.setImageURI(Uri.parse(ImageBasePath));

                    }
                })
                .setTitle("avatar")
                .setSelectMaxCount(1)
                .setSelectMinCount(1)
                .setPeekHeight(2600)
                .showTitle(false)
                .setCompleteButtonText(R.string.choose)
                .setEmptySelectionText(R.string.no_image)
                .create();
        bottomSheetDialogFragment.show(getSupportFragmentManager());
    }
    private void Pay(String path){
        showProgressDialog(getString(R.string.please_wait));
        MultipartBody.Part filePart = null;
        File ImageFile = new File(path);
        ProgressRequestBody fileBody = new ProgressRequestBody(ImageFile, PayActivity.this);
        filePart = MultipartBody.Part.createFormData("invoice", ImageFile.getName(), fileBody);
        RetroWeb.getClient().create(ServiceApi.class).pay(mLanguagePrefManager.getAppLanguage(),mSharedPrefManager.getUserData().getApi_token()
        ,bank_name.getText().toString(),account_owner.getText().toString(),account_number.getText().toString(),amount.getText().toString(),filePart).enqueue(
                new Callback<BaseResponse>() {
                    @Override
                    public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                        hideProgressDialog();
                        if (response.isSuccessful()){
                            if (response.body().getValue().equals("1")){
                                startActivity(new Intent(mContext,MainActivity.class));
                            }else {
                                CommonUtil.makeToast(mContext,response.body().getMsg());
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<BaseResponse> call, Throwable t) {
                        CommonUtil.handleException(mContext,t);
                        t.printStackTrace();
                        hideProgressDialog();

                    }
                }
        );
    }



    @Override
    public void onProgressUpdate(int percentage) {

    }

    @Override
    public void onError() {

    }

    @Override
    public void onFinish() {

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (data != null) {
            if (requestCode == Constant.RequestPermission.REQUEST_IMAGES){
                ArrayList<String> returnValue = data.getStringArrayListExtra(Pix.IMAGE_RESULTS);

                ImageBasePath =returnValue.get(0);
                add_image.setImageURI(Uri.parse(ImageBasePath));
            }
        }
    }
}
