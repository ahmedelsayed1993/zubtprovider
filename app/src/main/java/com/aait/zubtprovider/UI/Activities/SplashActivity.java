package com.aait.zubtprovider.UI.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;

import com.aait.zubtprovider.Base.ParentActivity;
import com.aait.zubtprovider.R;

import butterknife.BindView;

public class SplashActivity extends ParentActivity {

    /**
     * created by ahmed el_sayed  7/10/2019
     */

    @BindView(R.id.splash_lay)
    LinearLayout mLinearLayout;
    Animation fade;

    @Override
    protected void initializeComponents() {

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_splash;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }
    protected void onResume() {
        super.onResume();
        fade = AnimationUtils.loadAnimation(this, R.anim.alpha);
        mLinearLayout.clearAnimation();
        mLinearLayout.startAnimation(fade);
        fade.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(final Animation animation) {

            }

            @Override
            public void onAnimationEnd(final Animation animation) {
                if (mSharedPrefManager.getLoginStatus()) {
                   startActivity(new Intent(mContext, MainActivity.class));
                    SplashActivity.this.finish();
                }else {
                    startActivity(new Intent(mContext,LoginActivity.class));
                    SplashActivity.this.finish();
                }

            }

            @Override
            public void onAnimationRepeat(final Animation animation) {
            }
        });
    }

}

