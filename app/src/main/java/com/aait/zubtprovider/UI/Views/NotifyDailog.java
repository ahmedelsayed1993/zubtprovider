package com.aait.zubtprovider.UI.Views;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.Gravity;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import com.aait.zubtprovider.Pereferences.LanguagePrefManager;
import com.aait.zubtprovider.Pereferences.SharedPrefManager;
import com.aait.zubtprovider.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NotifyDailog extends Dialog {
    String  finishModel;
    Context mContext;
    SharedPrefManager mSharedPrefManager;
    LanguagePrefManager mLanguagePrefManager;
    @BindView(R.id.text)
    TextView text;

    public NotifyDailog(@NonNull Context context, String  text) {
        super(context);
        this.mContext=context;
        this.finishModel = text;
    }
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dailog_notify);
        getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        getWindow().setGravity(Gravity.CENTER);
        setCancelable(true);
        ButterKnife.bind(this);
        mSharedPrefManager = new SharedPrefManager(mContext);
        mLanguagePrefManager = new LanguagePrefManager(mContext);
        initializeComponents();
    }
    private void initializeComponents() {
        text.setText(finishModel);

    }

}

