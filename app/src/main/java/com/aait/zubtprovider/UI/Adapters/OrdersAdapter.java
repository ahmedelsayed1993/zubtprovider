package com.aait.zubtprovider.UI.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.aait.zubtprovider.Base.ParentRecyclerAdapter;
import com.aait.zubtprovider.Base.ParentRecyclerViewHolder;
import com.aait.zubtprovider.Models.OrderModel;
import com.aait.zubtprovider.Models.ReviewsModels;
import com.aait.zubtprovider.R;
import com.aait.zubtprovider.UI.Activities.AcceptOrderActivity;
import com.aait.zubtprovider.UI.Activities.OrderDetailsActivity;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

public class OrdersAdapter extends ParentRecyclerAdapter<OrderModel>{
    public OrdersAdapter(Context context, List<OrderModel> data, int layoutId) {
        super(context, data, layoutId);
    }

    @NonNull
    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(mcontext).inflate(layoutId, viewGroup, false);
        OrdersAdapter.ViewHolder holder = new OrdersAdapter.ViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ParentRecyclerViewHolder parentRecyclerViewHolder, final int i) {
        final OrdersAdapter.ViewHolder viewHolder = (OrdersAdapter.ViewHolder) parentRecyclerViewHolder;
        final OrderModel categoryModel = data.get(i);
        viewHolder.name.setText(categoryModel.getName());
        Glide.with(mcontext).load(categoryModel.getAvatar()).apply(new RequestOptions().placeholder(R.mipmap.untitled_6).fitCenter()).into(viewHolder.image);
        viewHolder.time.setText(categoryModel.getCreated_at());
        viewHolder.location.setText(categoryModel.getLocation());
        viewHolder.order_number.setText(mcontext.getString(R.string.order_number)+" : "+categoryModel.getId());
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (categoryModel.getStatus().equals("new_order")) {
                    Intent intent = new Intent(mcontext, AcceptOrderActivity.class);
                    intent.putExtra("id", categoryModel.getId() + "");
                    mcontext.startActivity(intent);
                }else {
                    Intent intent = new Intent(mcontext, OrderDetailsActivity.class);
                    intent.putExtra("id", categoryModel.getId() + "");
                    mcontext.startActivity(intent);
                }
            }
        });
    }
    public class ViewHolder extends ParentRecyclerViewHolder {

        @BindView(R.id.image)
        ImageView image;

        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.time)
        TextView time;
        @BindView(R.id.location)
        TextView location;
        @BindView(R.id.order_number)
                TextView order_number;





        ViewHolder(View itemView) {
            super(itemView);
        }

    }
}
