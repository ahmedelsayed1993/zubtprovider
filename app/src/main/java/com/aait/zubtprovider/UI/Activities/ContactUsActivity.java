package com.aait.zubtprovider.UI.Activities;

import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.aait.zubtprovider.App.Constant;
import com.aait.zubtprovider.Base.ParentActivity;
import com.aait.zubtprovider.Models.BaseResponse;
import com.aait.zubtprovider.Models.ContactInfoResponse;
import com.aait.zubtprovider.Network.RetroWeb;
import com.aait.zubtprovider.Network.ServiceApi;
import com.aait.zubtprovider.R;
import com.aait.zubtprovider.Uitls.CommonUtil;
import com.aait.zubtprovider.Uitls.PermissionUtils;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ContactUsActivity extends ParentActivity {
    @OnClick(R.id.act_back)
    void onBack(){
        onBackPressed();
    }
    @BindView(R.id.act_title)
    TextView act_title;
    @BindView(R.id.user_name)
    EditText user_name;
    @BindView(R.id.email)
    EditText email;
    @BindView(R.id.message)
    EditText message;
    @OnClick(R.id.notification)
    void onNotification(){
        startActivity(new Intent(mContext,NotificationActivity.class));
    }
    String phone="",whats="",face="",twitter="",insta = "";
    @Override
    protected void initializeComponents() {
        act_title.setText(getString(R.string.contact_us));
        getContacts();

    }
    @OnClick(R.id.phone)
    void onPhone(){
        if (!phone.equals("")) {
            getLocationWithPermission(phone);
        } else {
            CommonUtil.makeToast(mContext, getString(R.string.no));
        }
    }

    @OnClick(R.id.whats)
    void onWhats(){
        if (appInstalledOrNot(whats)) {
            try {
                Intent sendIntent = new Intent("android.intent.action.MAIN");
                sendIntent.setComponent(new ComponentName("com.whatsapp", "com.whatsapp.Conversation"));
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.setType("text/plain");
                sendIntent.putExtra(Intent.EXTRA_TEXT, "");
                sendIntent.putExtra("jid", whats + "@s.whatsapp.net");
                sendIntent.setPackage("com.whatsapp");
                startActivity(sendIntent);
            } catch (Exception e) {
                Toast.makeText(this, "Error/n" + e.toString(), Toast.LENGTH_SHORT).show();
            }

        }else mToaster.makeToast("Whats App is not installed");
    }
    private boolean appInstalledOrNot(String uri) {
        PackageManager pm = getPackageManager();
        boolean app_installed;
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            app_installed = true;
        }
        catch (PackageManager.NameNotFoundException e) {
            app_installed = false;
        }
        return app_installed;
    }
    @OnClick(R.id.face)
    void onFace(){
        if (!face.equals("")) {
            Log.e("here", "2222" );
            //     Toast.makeText(mContext, "hwwwwy", Toast.LENGTH_SHORT).show();
            if (face.startsWith("http")) {
                Log.e("here", "333" );
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(face));
                startActivity(i);

            } else {
                Log.e("here", "4444" );
                String url = "https://" + face;
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        }else {
            Log.e("here", "44444" );
        }
    }
    @OnClick(R.id.twitter)
    void onTwitter(){
        if (!twitter.equals("")) {
            if (twitter.startsWith("http")) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(twitter));
                startActivity(i);
            } else {
                String url = "https://" + twitter;
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        }
    }
    @OnClick(R.id.insta)
    void onInsta(){
        if (!insta.equals("")) {
            if (insta.startsWith("http")) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(insta));
                startActivity(i);
            } else {
                String url = "https://" + insta;
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        }
    }
    public void getLocationWithPermission(String number) {
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!PermissionUtils.hasPermissions(ContactUsActivity.this, PermissionUtils.CALL_PHONE)) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(PermissionUtils.CALL_PHONE,
                            Constant.RequestPermission.REQUEST_CALL);
                }
            } else {
                callnumber(number);
            }
        } else {
            callnumber(number);
        }

    }

    @Override
    public void onRequestPermissionsResult(int permsRequestCode, String[] permissions, int[] grantResults) {
        switch (permsRequestCode) {
            case 800: {
                if (grantResults.length > 0) {
                    boolean Locationpermission = (grantResults[0] == PackageManager.PERMISSION_GRANTED);
                    callnumber(phone);
                    for (int i = 0; i < grantResults.length; i++) {
                    }
                } else {
                }
                return;
            }
        }
    }
    void callnumber(String number) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + number));
        startActivity(intent);
    }
    @Override
    protected int getLayoutResource() {
        return R.layout.activity_contact_us;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }
    @OnClick(R.id.send)
    void onSend(){
        if (CommonUtil.checkTextError((AppCompatActivity)mContext,user_name,getString(R.string.enter_user_name))||
        CommonUtil.checkTextError((AppCompatActivity)mContext,email,getString(R.string.enter_email))||
                !CommonUtil.isEmailValid(email,getString(R.string.correct_email))||
        CommonUtil.checkTextError((AppCompatActivity)mContext,message,getString(R.string.write_message))){
            return;
        }else {
            ContactUs();
        }
    }
    private void getContacts(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getContacts().enqueue(new Callback<ContactInfoResponse>() {
            @Override
            public void onResponse(Call<ContactInfoResponse> call, Response<ContactInfoResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getValue().equals("1")){
                        phone = response.body().getData().getPhone();
                        whats = response.body().getData().getWhats_app();
                        twitter = response.body().getData().getTwitter();
                        face = response.body().getData().getFacebook();
                        insta = response.body().getData().getInstagram();

                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<ContactInfoResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    private void ContactUs(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).Contact(mLanguagePrefManager.getAppLanguage(),user_name.getText().toString(),email.getText().toString(),message.getText().toString()).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getValue().equals("1")){
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                        user_name.setText("");
                        email.setText("");
                        message.setText("");
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
}
