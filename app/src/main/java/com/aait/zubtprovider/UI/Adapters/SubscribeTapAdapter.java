package com.aait.zubtprovider.UI.Adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.aait.zubtprovider.R;
import com.aait.zubtprovider.UI.Fragments.CurrentOrdersFragment;
import com.aait.zubtprovider.UI.Fragments.FinishedOrdersFragment;



public class SubscribeTapAdapter extends FragmentPagerAdapter {

    private Context context;

    public SubscribeTapAdapter(Context context , FragmentManager fm) {
        super(fm);
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0){
            return new CurrentOrdersFragment();
        }else {
            return new FinishedOrdersFragment();
        }
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (position==0){
            return context.getString(R.string.orders_under_process);
        }else {
            return context.getString(R.string.finished_orders);
        }
    }
}
