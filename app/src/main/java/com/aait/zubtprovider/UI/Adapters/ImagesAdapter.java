package com.aait.zubtprovider.UI.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.aait.zubtprovider.Base.ParentRecyclerAdapter;
import com.aait.zubtprovider.Base.ParentRecyclerViewHolder;
import com.aait.zubtprovider.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import butterknife.BindView;

public class ImagesAdapter extends ParentRecyclerAdapter<String> {
    public ImagesAdapter(Context context, List<String> data, int layoutId) {
        super(context, data, layoutId);
    }

    @NonNull
    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(mcontext).inflate(layoutId, viewGroup, false);
        ImagesAdapter.ViewHolder holder = new ImagesAdapter.ViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ParentRecyclerViewHolder parentRecyclerViewHolder, final int i) {
        final ImagesAdapter.ViewHolder viewHolder = (ImagesAdapter.ViewHolder) parentRecyclerViewHolder;
        final String categoryModel = data.get(i);

        Glide.with(mcontext).load(categoryModel).apply(new RequestOptions().placeholder(R.mipmap.untitled_6).fitCenter()).into(viewHolder.image);


    }
    public class ViewHolder extends ParentRecyclerViewHolder {

        @BindView(R.id.image)
        ImageView image;






        ViewHolder(View itemView) {
            super(itemView);
        }

    }
}
