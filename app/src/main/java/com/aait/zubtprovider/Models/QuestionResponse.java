package com.aait.zubtprovider.Models;

public class QuestionResponse extends BaseResponse{
    private QuestionsResponse data;

    public QuestionsResponse getData() {
        return data;
    }

    public void setData(QuestionsResponse data) {
        this.data = data;
    }
}
