package com.aait.zubtprovider.Models;

public class OrdersResponse extends BaseResponse {
    private OrdersModel data;

    public OrdersModel getData() {
        return data;
    }

    public void setData(OrdersModel data) {
        this.data = data;
    }
}
