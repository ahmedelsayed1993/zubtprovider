package com.aait.zubtprovider.Models;

import java.io.Serializable;

public class PaginationModel implements Serializable {
    private int total;
    private int count;
    private int per_page;
    private String next_page_url;
    private String perv_page_url;
    private int current_page;
    private int total_pages;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getPer_page() {
        return per_page;
    }

    public void setPer_page(int per_page) {
        this.per_page = per_page;
    }

    public String getNext_page_url() {
        return next_page_url;
    }

    public void setNext_page_url(String next_page_url) {
        this.next_page_url = next_page_url;
    }

    public String getPerv_page_url() {
        return perv_page_url;
    }

    public void setPerv_page_url(String perv_page_url) {
        this.perv_page_url = perv_page_url;
    }

    public int getCurrent_page() {
        return current_page;
    }

    public void setCurrent_page(int current_page) {
        this.current_page = current_page;
    }

    public int getTotal_pages() {
        return total_pages;
    }

    public void setTotal_pages(int total_pages) {
        this.total_pages = total_pages;
    }
}
