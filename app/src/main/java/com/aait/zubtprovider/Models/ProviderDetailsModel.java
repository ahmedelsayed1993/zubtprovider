package com.aait.zubtprovider.Models;

import java.io.Serializable;
import java.util.ArrayList;

public class ProviderDetailsModel implements Serializable {
    private int id;
    private String name;
    private String avatar;
    private String city;
    private float rate;
    private String work_from;
    private String work_to;
    private String details;
    private int is_favourite;
    private int is_reported;
    private String location;
    private ArrayList<ReviewsModels> reviews;

    public ArrayList<ReviewsModels> getReviews() {
        return reviews;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setReviews(ArrayList<ReviewsModels> reviews) {
        this.reviews = reviews;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public float getRate() {
        return rate;
    }

    public void setRate(float rate) {
        this.rate = rate;
    }

    public String getWork_from() {
        return work_from;
    }

    public void setWork_from(String work_from) {
        this.work_from = work_from;
    }

    public String getWork_to() {
        return work_to;
    }

    public void setWork_to(String work_to) {
        this.work_to = work_to;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public int getIs_favourite() {
        return is_favourite;
    }

    public void setIs_favourite(int is_favourite) {
        this.is_favourite = is_favourite;
    }

    public int getIs_reported() {
        return is_reported;
    }

    public void setIs_reported(int is_reported) {
        this.is_reported = is_reported;
    }
}
