package com.aait.zubtprovider.Models;

import java.io.Serializable;

public class ContactInfoModel implements Serializable {
    private String phone;
    private String whats_app;
    private String facebook;
    private String twitter;
    private String instagram;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getWhats_app() {
        return whats_app;
    }

    public void setWhats_app(String whats_app) {
        this.whats_app = whats_app;
    }

    public String getFacebook() {
        return facebook;
    }

    public void setFacebook(String facebook) {
        this.facebook = facebook;
    }

    public String getTwitter() {
        return twitter;
    }

    public void setTwitter(String twitter) {
        this.twitter = twitter;
    }

    public String getInstagram() {
        return instagram;
    }

    public void setInstagram(String instagram) {
        this.instagram = instagram;
    }
}
