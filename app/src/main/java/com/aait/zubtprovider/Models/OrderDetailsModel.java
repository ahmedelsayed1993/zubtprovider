package com.aait.zubtprovider.Models;

import java.io.Serializable;
import java.util.ArrayList;

public class OrderDetailsModel implements Serializable {
    private int id;
    private float cost;
    private String status;
    private String category;
    private String date;
    private String time;
    private String details;
    private String location;
    private String lat;
    private String lng;
    private int include_material;
    private int include_tax;
    private String client_phone;
    private String client_email;
    private ArrayList<String> images;

    public String getClient_email() {
        return client_email;
    }

    public void setClient_email(String client_email) {
        this.client_email = client_email;
    }

    public String getClient_phone() {
        return client_phone;
    }

    public void setClient_phone(String client_phone) {
        this.client_phone = client_phone;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getCost() {
        return cost;
    }

    public void setCost(float cost) {
        this.cost = cost;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public int getInclude_material() {
        return include_material;
    }

    public void setInclude_material(int include_material) {
        this.include_material = include_material;
    }

    public int getInclude_tax() {
        return include_tax;
    }

    public void setInclude_tax(int include_tax) {
        this.include_tax = include_tax;
    }

    public ArrayList<String> getImages() {
        return images;
    }

    public void setImages(ArrayList<String> images) {
        this.images = images;
    }
}
