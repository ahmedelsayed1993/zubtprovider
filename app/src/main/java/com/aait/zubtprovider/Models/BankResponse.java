package com.aait.zubtprovider.Models;

import java.util.ArrayList;

public class BankResponse extends BaseResponse{
    private ArrayList<Bank_model> data;

    public ArrayList<Bank_model> getData() {
        return data;
    }

    public void setData(ArrayList<Bank_model> data) {
        this.data = data;
    }
}
