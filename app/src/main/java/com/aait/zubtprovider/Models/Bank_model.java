package com.aait.zubtprovider.Models;

import java.io.Serializable;

public class Bank_model implements Serializable {
    private String bank_name;
    private String account_name;
    private String account_id;
    private String iban_id;

    public String getBank_name() {
        return bank_name;
    }

    public void setBank_name(String bank_name) {
        this.bank_name = bank_name;
    }

    public String getAccount_name() {
        return account_name;
    }

    public void setAccount_name(String account_name) {
        this.account_name = account_name;
    }

    public String getAccount_id() {
        return account_id;
    }

    public void setAccount_id(String account_id) {
        this.account_id = account_id;
    }

    public String getIban_id() {
        return iban_id;
    }

    public void setIban_id(String iban_id) {
        this.iban_id = iban_id;
    }
}
