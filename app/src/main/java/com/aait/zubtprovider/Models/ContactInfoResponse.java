package com.aait.zubtprovider.Models;

public class ContactInfoResponse extends BaseResponse {
    private ContactInfoModel data;

    public ContactInfoModel getData() {
        return data;
    }

    public void setData(ContactInfoModel data) {
        this.data = data;
    }
}
