package com.aait.zubtprovider.Models;

import java.io.Serializable;

public class RegisterModel implements Serializable {
    private String api_token;
    private String code;

    public String getApi_token() {
        return api_token;
    }

    public void setApi_token(String api_token) {
        this.api_token = api_token;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
