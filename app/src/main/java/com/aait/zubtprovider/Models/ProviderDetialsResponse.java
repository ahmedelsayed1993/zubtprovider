package com.aait.zubtprovider.Models;

public class ProviderDetialsResponse extends BaseResponse {
    private ProviderDetailsModel data;

    public ProviderDetailsModel getData() {
        return data;
    }

    public void setData(ProviderDetailsModel data) {
        this.data = data;
    }
}
