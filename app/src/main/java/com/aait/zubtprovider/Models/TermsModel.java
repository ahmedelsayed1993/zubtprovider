package com.aait.zubtprovider.Models;

import java.io.Serializable;

public class TermsModel implements Serializable {
    private String about;
    private String terms;

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getTerms() {
        return terms;
    }

    public void setTerms(String terms) {
        this.terms = terms;
    }
}
