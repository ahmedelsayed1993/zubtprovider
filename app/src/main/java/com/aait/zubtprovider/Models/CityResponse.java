package com.aait.zubtprovider.Models;

import java.util.ArrayList;

public class CityResponse extends BaseResponse {
    private ArrayList<CityModel> data;

    public ArrayList<CityModel> getData() {
        return data;
    }

    public void setData(ArrayList<CityModel> data) {
        this.data = data;
    }
}
