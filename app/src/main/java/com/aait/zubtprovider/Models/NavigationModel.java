package com.aait.zubtprovider.Models;

/**
 * Created by Ahmed El_sayed on 5/25/19.
 */

public class NavigationModel {
    private int natIcon;

    private String navTitle;

    public NavigationModel(String navTitle, int natIcon) {
        this.navTitle = navTitle;
        this.natIcon = natIcon;
    }

    public int getNatIcon() {
        return natIcon;
    }
    public String getNavTitle() {
        return navTitle;
    }
}
