package com.aait.zubtprovider.Models;

public class NotificationResponse extends BaseResponse{
    private NotificationsModel data;

    public NotificationsModel getData() {
        return data;
    }

    public void setData(NotificationsModel data) {
        this.data = data;
    }
}
