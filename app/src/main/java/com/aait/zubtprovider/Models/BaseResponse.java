package com.aait.zubtprovider.Models;

import java.io.Serializable;

public class BaseResponse implements Serializable {
    private String key;
    private String value;
    private String msg;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
    public boolean issucessfull(){
        if (value.equals("1")){
            return true;
        }else {
            return false;
        }
    }
}
