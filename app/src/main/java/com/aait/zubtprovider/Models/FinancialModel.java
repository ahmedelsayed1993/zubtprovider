package com.aait.zubtprovider.Models;

import java.io.Serializable;

public class FinancialModel implements Serializable {
    private float balance;

    public float getBalance() {
        return balance;
    }

    public void setBalance(float balance) {
        this.balance = balance;
    }
}
