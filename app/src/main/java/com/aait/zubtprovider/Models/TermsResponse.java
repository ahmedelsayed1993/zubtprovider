package com.aait.zubtprovider.Models;

public class TermsResponse extends BaseResponse {
    private TermsModel data;

    public TermsModel getData() {
        return data;
    }

    public void setData(TermsModel data) {
        this.data = data;
    }
}
