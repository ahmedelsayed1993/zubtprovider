package com.aait.zubtprovider.Models;

public class OrderDetailsResponse extends BaseResponse {
    private OrderDetailsModel data;

    public OrderDetailsModel getData() {
        return data;
    }

    public void setData(OrderDetailsModel data) {
        this.data = data;
    }
}
