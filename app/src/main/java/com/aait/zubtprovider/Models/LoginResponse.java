package com.aait.zubtprovider.Models;

public class LoginResponse extends BaseResponse{
    private UserModel data;
    private String status;

    public UserModel getData() {
        return data;
    }

    public void setData(UserModel data) {
        this.data = data;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
