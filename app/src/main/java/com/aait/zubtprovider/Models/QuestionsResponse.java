package com.aait.zubtprovider.Models;

import java.io.Serializable;
import java.util.ArrayList;

public class QuestionsResponse implements Serializable {
    private ArrayList<QuestionModel> questions;
    private PaginationModel pagination;

    public ArrayList<QuestionModel> getQuestions() {
        return questions;
    }

    public void setQuestions(ArrayList<QuestionModel> questions) {
        this.questions = questions;
    }

    public PaginationModel getPagination() {
        return pagination;
    }

    public void setPagination(PaginationModel pagination) {
        this.pagination = pagination;
    }
}
