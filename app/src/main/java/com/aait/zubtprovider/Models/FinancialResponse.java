package com.aait.zubtprovider.Models;

public class FinancialResponse extends BaseResponse{
    private FinancialModel data;

    public FinancialModel getData() {
        return data;
    }

    public void setData(FinancialModel data) {
        this.data = data;
    }
}
