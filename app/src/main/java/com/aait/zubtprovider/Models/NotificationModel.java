package com.aait.zubtprovider.Models;

import java.io.Serializable;

public class NotificationModel implements Serializable {
    private int id;
    private String data;
    private String type;
    private String  order_id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String  getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String  order_id) {
        this.order_id = order_id;
    }
}
