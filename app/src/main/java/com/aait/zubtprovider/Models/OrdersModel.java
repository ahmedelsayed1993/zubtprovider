package com.aait.zubtprovider.Models;

import java.io.Serializable;
import java.util.ArrayList;

public class OrdersModel implements Serializable {
    private int notifications;
    private ArrayList<OrderModel> orders;
    private PaginationModel pagination;

    public int getNotifications() {
        return notifications;
    }

    public void setNotifications(int notifications) {
        this.notifications = notifications;
    }

    public ArrayList<OrderModel> getOrders() {
        return orders;
    }

    public void setOrders(ArrayList<OrderModel> orders) {
        this.orders = orders;
    }

    public PaginationModel getPagination() {
        return pagination;
    }

    public void setPagination(PaginationModel pagination) {
        this.pagination = pagination;
    }
}
