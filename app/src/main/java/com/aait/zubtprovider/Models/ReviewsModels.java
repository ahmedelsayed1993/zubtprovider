package com.aait.zubtprovider.Models;

import java.io.Serializable;

public class ReviewsModels implements Serializable {
    private int id;
    private String name;
    private String avatar;
    private String comment;
    private int is_reported;
    private String created_at;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getIs_reported() {
        return is_reported;
    }

    public void setIs_reported(int is_reported) {
        this.is_reported = is_reported;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }
}
