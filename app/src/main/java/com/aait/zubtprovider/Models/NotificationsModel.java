package com.aait.zubtprovider.Models;

import java.io.Serializable;
import java.util.ArrayList;

public class NotificationsModel implements Serializable {
    private ArrayList<NotificationModel> notifications;
    private PaginationModel pagination;

    public ArrayList<NotificationModel> getNotifications() {
        return notifications;
    }

    public void setNotifications(ArrayList<NotificationModel> notifications) {
        this.notifications = notifications;
    }

    public PaginationModel getPagination() {
        return pagination;
    }

    public void setPagination(PaginationModel pagination) {
        this.pagination = pagination;
    }
}
