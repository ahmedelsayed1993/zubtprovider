package com.aait.zubtprovider.FCM;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.aait.zubtprovider.Pereferences.LanguagePrefManager;
import com.aait.zubtprovider.Pereferences.SharedPrefManager;
import com.aait.zubtprovider.R;

import com.aait.zubtprovider.UI.Activities.MainActivity;
import com.aait.zubtprovider.UI.Activities.NotificationActivity;
import com.aait.zubtprovider.UI.Activities.RegisterActivity;
import com.aait.zubtprovider.UI.Activities.SplashActivity;
import com.aait.zubtprovider.Uitls.CommonUtil;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;

import java.util.List;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    SharedPreferences sharedPreferences;
    private static final String TAG = "FCM Messaging";

    String notificationTitle;

    String notificationMessage;

    String notificationData;

    SharedPrefManager mSharedPrefManager;

    String notificationType;

    // OrderModel mOrderModel;

    LanguagePrefManager mLanguagePrefManager;

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        Log.e("NEW_TOKEN", s);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        sharedPreferences = getSharedPreferences("home", MODE_PRIVATE);
        CommonUtil.onPrintLog(remoteMessage.getNotification());
        CommonUtil.onPrintLog(remoteMessage.getData());
        mSharedPrefManager = new SharedPrefManager(getApplicationContext());
        mLanguagePrefManager = new LanguagePrefManager(getApplicationContext());

        if (!mSharedPrefManager.getNotificationStatus()) {
            return;
        }


//        notificationType = remoteMessage.getData().get("type");
        notificationTitle = remoteMessage.getData().get("title");
        notificationMessage = remoteMessage.getData().get("description");
        notificationType = remoteMessage.getData().get("notification_type");
        // if the notification contains data payload
        if (remoteMessage == null) {
            return;
        }

        // if the user not logged in never do any thing
        if (!mSharedPrefManager.getLoginStatus()) {
            return;
        }  else {
            if (remoteMessage.getData().get("status").equals("user_block")){
                mSharedPrefManager.setLoginStatus(false);
                mSharedPrefManager.Logout();
                startActivity(new Intent(getApplicationContext(), SplashActivity.class));
            }
                Intent intent = new Intent(this, NotificationActivity.class);
                intent.putExtra("notification", "notification");
                showNotification(remoteMessage, intent);
            }
    }

    private void showNotification(RemoteMessage message, Intent intent) {

        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);


        PendingIntent pendingIntent = PendingIntent.getActivity(
                this,
                0,
                intent,
                PendingIntent.FLAG_ONE_SHOT);

        NotificationCompat.Builder myNotification = new NotificationCompat.Builder(this)
                .setContentTitle(message.getData().get("title"))
                .setContentText(message.getData().get("body"))
                .setTicker("Notification!")
                .setWhen(System.currentTimeMillis())
                .setContentIntent(pendingIntent)
                .setDefaults(Notification.DEFAULT_SOUND)
                .setAutoCancel(true)
                .setPriority(Notification.PRIORITY_MAX)
                .setSmallIcon(R.mipmap.untitled_1);
        NotificationManager notificationManager =
                (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = new NotificationChannel("10001", "NOTIFICATION_CHANNEL_NAME", importance);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.DKGRAY);
            notificationChannel.setShowBadge(true);
            notificationChannel.enableVibration(true);
            notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            myNotification.setChannelId("10001");
            notificationManager.createNotificationChannel(notificationChannel);
        }
        notificationManager.notify(0 /* Request Code */, myNotification.build());
    }

    /**
     * Processing user specific push message
     * It will be displayed with / without image in push notification tray
     */
    public boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = false;
            }
        }

        return isInBackground;
    }

}
